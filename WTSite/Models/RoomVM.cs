﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WTSite.Models
{
    public class PrioritizationVM
    {
        public int[] PrioritizationID { get; set; }
        public MultiSelectList Prioritizations { get; set; }
        public int ChildId { get; set; }
    }

    public class RoomVM
    {
        public room Room { get; set; }
        //[Remote("ValidateDateEqualOrGreater", HttpMethod ="Post", ErrorMessage = "Date must be greater or equal to today's date.")]
        public DateTime? WaitlistOverrideDate { get; set; }
        public List<ChildRoster> RosterChildren { get; set; }
        public List<ChildWaitlist> WaitlistChildren { get; set; }
        public List<prioritization> Prioritizations { get; set; }
        public DateTime? EEDWithoutPrioritization { get; set; }
        public List<PrioritizationEED> EEDsWithPrioritization { get; set; }
        public List<PrioritizationVM> ChildrenPrioritizationVMs { get; set; }

        public RoomVM() { }

        public RoomVM(room room,
            DateTime? waitlistoverridedate,
            List<ChildRoster> rosterchildren = null,
            List<ChildWaitlist> waitlistchildren = null,
            List<prioritization> prioritizations = null,
            DateTime? eedwithoutprioritization = null,
            List<PrioritizationEED> eedswithprioritization = null,
            List<MultiSelectList> childrensPrioritizations = null,
            MultiSelectList a = null)
        {
            Room = room;
            WaitlistOverrideDate = waitlistoverridedate;
            Prioritizations = prioritizations;
            RosterChildren = rosterchildren;
            WaitlistChildren = waitlistchildren;
            if (eedwithoutprioritization == null)
            {
                EEDWithoutPrioritization = DateTime.Today;
            }
            else
            {
                EEDWithoutPrioritization = eedwithoutprioritization;
            }
            EEDsWithPrioritization = eedswithprioritization;

        }
    }
}