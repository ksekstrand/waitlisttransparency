﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    public class ChildRoster
    {
        public int ChildId { get; set; }
        [Required(ErrorMessage = "Please enter an identifier for the child")]
        [StringLength(15, ErrorMessage = "Identifier cannot exceed 15 characters")]
        [RegularExpression("^[0-9a-zA-Z]+$", ErrorMessage = "Identifier must contain letters and numbers only with no spaces")]
        public string ChildName { get; set; }
        [Required(ErrorMessage = "Please enter in a date of birth for the child")]
        public DateTime DOB { get; set; }
        [MaxLength(200, ErrorMessage = "Please enter 200 character or less")]
        public String ChildComments { get; set; }
        public DateTime? OverrideDate { get; set; }
        public DateTime? DateAdded { get; set; }

        public ChildRoster() { }

        public ChildRoster(int childid, string childname, DateTime dob, DateTime? overridedate, DateTime? dateAdded, string childComments = "")
        {
            ChildId = childid;
            if (childname == null) 
            {
                ChildName = childid.ToString(); //validation shouldn't allow null, but this is an added safeguard to default the child identifier to their id num
            }
            else
            {
                ChildName = childname;
            }
            DOB = dob;
            //if (overridedate < DateTime.Today)
            //{
            //    OverrideDate = DateTime.Today;
            //}
            //else
            //{
            //    OverrideDate = overridedate;
            //}
            OverrideDate = overridedate;
            ChildComments = childComments;
          
            DateAdded = dateAdded;
            
        }
    }
}