﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WTSite.Models
{
    public class ChildWaitlist
    {
        private WTEntities db = new WTEntities();

        public int ChildId { get; set; }

        [Required(ErrorMessage = "Please enter an identifier for the child")]
        [StringLength(15, ErrorMessage = "Identifier cannot exceed 15 characters")]
        [RegularExpression("^[0-9a-zA-Z]+$", ErrorMessage = "Identifier must contain letters and numbers only with no spaces")]
        public string ChildName { get; set; }

        [Required(ErrorMessage = "Please enter in a date of birth for the child")]
        public DateTime DOB { get; set; }

        [MaxLength(200, ErrorMessage = "Please enter 200 character or less")]
        public String ChildComments { get; set; }

        public DateTime? DateAdded { get; set; }
        public int HighestPrioritizationRank { get; set; }
        public List<prioritization> Prioritizations { get; set; }
        public DateTime EED { get; set; }

        public ChildWaitlist() { }

        public ChildWaitlist(int childid)
        {
            ChildId = childid;
        }

        public ChildWaitlist(int childid, string childname, DateTime dob, DateTime? dateadded, DateTime eed,
            string childComments = "")
        {
            ChildId = childid;
            ChildName = childname;
            DOB = dob;
            if (dateadded == null)
                DateAdded = null;
            else
                DateAdded = dateadded;

            EED = eed;

            // retrives all the child's proritizations and adds them to Prioritizations 
            child child = db.children.Single(item => item.child_id == ChildId);
            Prioritizations = child.prioritizations.ToList();

            // set HighestPrioritizationRank, used for waitlist sorting purposes in RoomsController
            if (Prioritizations.Count > 0)
            {
                HighestPrioritizationRank = (from item in Prioritizations
                                             select item.priority_rank).Min();
            }
            else
            {
                HighestPrioritizationRank = 1000;
            }

            ChildComments = childComments;
        }
    }
}