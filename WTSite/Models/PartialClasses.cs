﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WTSite.Models
{
    [MetadataType(typeof(childMetadata))]
    public partial class child { }

    [MetadataType(typeof(childcare_facilityMetadata))]
    public partial class childcare_facility { }

    [MetadataType(typeof(childcare_facility_detailMetadata))]
    public partial class childcare_facility_detail { }

    [MetadataType(typeof(detailMetadata))]
    public partial class detail { }

    [MetadataType(typeof(prioritizationMetata))]
    public partial class prioritization { }

    [MetadataType(typeof(roomMetata))]
    public partial class room { }

    [MetadataType(typeof(waitlistMetadata))]
    public partial class waitlist { }

    [MetadataType(typeof(waitlist_childMetadata))]
    public partial class waitlist_child { }

}