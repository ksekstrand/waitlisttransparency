﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WTSite.Models
{
    public class childMetadata
    {
        public int child_id ;
        //put child_name dataannotations here?
        [Required(ErrorMessage = "Please enter an identifier for the child")]
        [StringLength(15, ErrorMessage = "Identifier cannot exceed 15 characters")]
        [RegularExpression("^[0-9a-zA-Z]+$", ErrorMessage = "Identifier must contain letters and numbers only with no spaces")]
        public string child_name ;
        [Required(ErrorMessage = "Please enter a date of birth for the child")]
        [DataType(DataType.Date)]
        public System.DateTime child_dob ;
        public Nullable<System.DateTime> child_override_date ;
        [Required]
        public int room_id ;
    }

    public class childcare_facilityMetadata
    {
        public int childcare_facility_id ;
        [Required]
        [Display(Name = "Child Care Facility Name")]
        [StringLength(100, ErrorMessage = "Child care facility name cannot exceed 100 characters")]
        public string childcare_facility_name ;
        [Required]
        [Display(Name = "Address Line 1")]
        [StringLength(60, ErrorMessage = "Address line 1 cannot exceed 60 characters.")]
        public string address_line_1 ;
        [Display(Name = "Address Line 2")]
        [StringLength(60, ErrorMessage = "Address line 2 cannot exceed 60 characters.")]
        public string address_line_2 ;
        [Required]
        [Display(Name = "City")]
        [StringLength(40, ErrorMessage = "City cannot exceed 40 characters.")]
        public string city ;
        [Required]
        [Display(Name = "State")]
        [StringLength(2, ErrorMessage = "Please enter the state's two digit code.")]
        public string state ;
        [Required]
        [StringLength(5, ErrorMessage = "Please enter a 5-digit ZIP code.")]
        [Display(Name = "Zip Code")]
        public string zip_code ;
        [Display(Name = "Phone Number")]
        [StringLength(13, ErrorMessage = "Phone number cannot exceed 13 characters.")]
        //[RegularExpression(@"^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$")]
        //[StringLength(13, ErrorMessage = "Please enter a valid phone number.")]
        public string phone_number ;
        //[Required(ErrorMessage = "Please enter your email address")] //validation will be handled by Identity, needs to be removed here for Registration page to work with this model.
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [MaxLength(50)]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        //[RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter a valid email address")]
        public string email_address ;
        [Display(Name = "Website")]
        [StringLength(255, ErrorMessage = "Website cannot exceed 255 characters.")]
        public string website ;
        [Display(Name = "Primary Contact")]
        [StringLength(40, ErrorMessage = "Primary contact cannot exceed 255 characters.")]
        public string primary_contact ;
        [Required]
        [Display(Name = "Facility License Number")]
        [StringLength(255, ErrorMessage = "Facility license number cannot exceed 255 characters.")]
        public string childcare_facility_license_number ;
        [Required]
        [Display(Name = "Facility License Expiration")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime childcare_facility_license_expiration ;
        public byte account_activated ;
        public string user_id ;
    }

    public class childcare_facility_detailMetadata
    {
        [Display(Name = "Facility ID")]
        public int childcare_facility_id ;
        [Display(Name = "Detail ID")]
        public int detail_id ;
        [Display(Name = "Detail Yes/No")]
        [Required]
        public byte detail_yes_no ;
        [Display(Name = "Detail Comment")]
        public string detail_comment ;
        
    }

    public class detailMetadata
    {
        [Required]
        [Display(Name = "Detail ID")]
        public int detail_id ;
        [Required]
        [Display(Name = "Detail Name")]
        [StringLength(200, ErrorMessage = "Detail Name too long.")]
        public string detail_name ;
    }

    public class prioritizationMetata
    {
        [Required]
        [Display(Name = "Prioritization ID")]
        public int prioritization_id ;
        [Required(ErrorMessage = "Please enter a prioiritization name")]
        [Display(Name = "Priority Name")]
        [StringLength(255, ErrorMessage = "Prioritization name cannot exceed 255 characters")]
        public string priority_name ;
        [Required]
        [Display(Name = "Priority Rank")]
        [Range(0, 100)] // this is higher than the website should allow
        public int priority_rank ;
        [Required]
        public int childcare_facility_id ;
    }      
    
    public class roomMetata
    {
        [Required]
        public int room_id ;
        [Required(ErrorMessage = "Please enter a room name")]
        [Display(Name = "Room Name")]
        [StringLength(60, ErrorMessage = "Room name cannot exceed 60 characters")]
        public string room_name ;
        [Required(ErrorMessage = "Please enter a room age minimum")]
        [Display(Name = "Room Age Min")]
        [Range(0, 83)]
        public int room_age_range_min ;
        [Required(ErrorMessage = "Please enter a room age maximum")]
        [Display(Name = "Room Age Max")]
        [Range(0, 83)]
        public int room_age_range_max ;
        [Required(ErrorMessage = "Please enter a room capacity")]
        [Display(Name = "Room Capacity")]
        [Range(1, 100)]
        public int room_capacity ;
        [Required]
        public int childcare_facility_id ;
    } 

    public class waitlistMetadata
    {
        [Required]
        public int waitlist_id ;
        public Nullable<System.DateTime> waitlist_override_date ;
        [Required]
        public int room_id ;
    }

    public class waitlist_childMetadata
    {
        [Required]
        public int waitlist_id ;
        [Required]
        public int child_id ;
        [Required]
        [DataType(DataType.Date)]
        public System.DateTime date_added ;
    }
}