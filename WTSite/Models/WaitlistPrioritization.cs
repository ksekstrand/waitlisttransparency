﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    // THIS WHOLE CLASS IS PLAYING AROUND FOR THE DROP DOWN CHECKLIST ON THE ROOM'S PAGE, MAY BE DELETED 

    public class WaitlistPrioritization
    {
        public int PrioritizationID { get; set; }
        public string PrioritizationName { get; set; }
        public int PrioritizationRank { get; set; }
        public bool IsSelected { get; set; }
        public object Tags { get; set; }
    }

    public class WaitlistPrioritizationVM
    {
        public IEnumerable<WaitlistPrioritization> AvailableWaitlistPrioritizations { get; set; }
        public IEnumerable<WaitlistPrioritization> SelectedWaitlistPrioritizations { get; set; }
        public PostedWaitlistPrioritizations PostedWaitlistPrioritizations { get; set; }
    }

    /// for Helper class to make posting back selected values easier
    public class PostedWaitlistPrioritizations
    {
        //this array will be used to POST values from the form to the controller
        public string[] PrioritizationIds { get; set; }
    }
}