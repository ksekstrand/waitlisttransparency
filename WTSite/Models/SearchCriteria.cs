﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    public class SearchCriteria
    {
        [StringLength(40, ErrorMessage = "The city cannot exceed 40 characters")]
        public string City { get; set; }
        [StringLength(2, ErrorMessage = "The state or territory's two digit code cannot exceed 2 characters")]
        public string State { get; set; }
        [StringLength(5, ErrorMessage = "The zip code cannot exceed 5 characters")]
        [DisplayName("ZIP Code")]
        public string ZipCode { get; set; }
        [Required(ErrorMessage = "Please enter a child's expected or actual date of birth")]
        [DisplayName("Child's Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        public SearchCriteria() { }

        public SearchCriteria(DateTime dob, string city, string state)
        {
            DOB = dob;
            City = city;
            State = state;
        }

        public SearchCriteria(DateTime dob, string zipcode)
        {
            DOB = dob;
            ZipCode = zipcode;
        }
    }
}