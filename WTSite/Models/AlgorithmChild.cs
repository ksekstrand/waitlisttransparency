﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Itenso.TimePeriod; // Install-Package TimePeriodLibrary.NET

namespace WTSite.Models
{
    public class AlgorithmChild
    {
        public int ChildID { get; set; } // will always initialize to 0, which is perfect because no child's ID will ever be 0
        public DateTime DOB { get; set; }
        public DateTime OverrideDate { get; set; }
        public int PrioritizationRank { get; set; }
        public DateTime LeaveDate { get; set; }

        DateTime currentDate = DateTime.Today;

        // constructor used for the room page, CNC without prioritization
        public AlgorithmChild()
        {
            PrioritizationRank = 1000;
            OverrideDate = currentDate;
        }

        // constructor used for the room page, CNC with prioritization
        public AlgorithmChild(int prioritizationrank)
        {
            PrioritizationRank = prioritizationrank;
        }

        // constructor used for the search page
        public AlgorithmChild(DateTime dob)
        {
            DOB = dob;
            PrioritizationRank = 1000;
            OverrideDate = currentDate;
        }

        // constructor used for the waitlist children
        public AlgorithmChild(int childid, DateTime dob, int prioitizationrank = 1000)
        {
            ChildID = childid;
            DOB = dob;
            PrioritizationRank = prioitizationrank;
            OverrideDate = currentDate;
        }

        // constructor used for the roster children
        public AlgorithmChild(int childid, DateTime dob, DateTime overridedate)
        {
            ChildID = childid;
            DOB = dob;
            OverrideDate = overridedate;
        }

        public int calculateMonthsOld(DateTime enrollmentdate)
        {
            if (DOB != null)
            {
                // Install-Package TimePeriodLibrary.NET
                DateDiff dateDiff = new DateDiff(DOB, enrollmentdate);

                //temporary, will probably not return an array in the future
                //int[] ageArray = new int[2] { dateDiff.ElapsedMonths + (12 * dateDiff.ElapsedYears), dateDiff.ElapsedDays };
                int age = dateDiff.ElapsedMonths + (12 * dateDiff.ElapsedYears);
                return age;
            }
            // if the child does not have a DOB, this is only ever the case when calculating the EED to display within the bottom of the room page, return a place holder of 0
            return 0;
        }

        public DateTime calculateAgingOut(int ageOut)
        {
            DateTime today = DateTime.Today;

            if (DOB != null)
            {
                DateDiff currentAge = new DateDiff(DOB, today); //not really applicable

                // if child has override date
                if (this.OverrideDate != this.currentDate)
                {
                    // return overridedate
                    LeaveDate = this.OverrideDate; //maybe problems
                    return this.OverrideDate;
                }
                // else, if current age is equal to, or greater than, the room's age max (meaning the rostered child has already aged out of the room)
                else if ((currentAge.ElapsedMonths + (currentAge.ElapsedYears * 12)) > ageOut)
                {
                    // return the current date
                    LeaveDate = today;
                    return today;
                }
                // else, calculate the date the child will age out
                else
                {
                    DateDiff agingOut = new DateDiff(DOB, today);

                    while ((agingOut.ElapsedMonths + (agingOut.ElapsedYears * 12)) <= ageOut)
                    {
                        today = today.AddDays(1);

                        agingOut = new DateDiff(DOB, today);
                    }
                    // return the date the child reaches the room's age max
                    LeaveDate = today;
                    return today;
                }
            }
            // if the child does not have a DOB, this is only ever the case when calculating the EED to display within the bottom of the room page, return a place holder of Today
            return today;
        }
    }
}