﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    public class PrioritizationEED
    {
        public prioritization Prioritization { get; set; }
        public DateTime EED { get; set; }

        public PrioritizationEED() { }

        public PrioritizationEED(prioritization prioritization, DateTime prioritizationeed)
        {
            Prioritization = prioritization;
            EED = prioritizationeed;
        }
    }
}