﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WTSite.Models
{
    public class FacilityDetail
    {
        public int FacilityID { get; set; }
        public int DetailID { get; set; }
        public string DetailName { get; set; }
        [Range(0,2)]
        public byte? DetailYesNo { get; set; }
        public string DetailComment { get; set; }

        public FacilityDetail() { }

        public FacilityDetail(int facilityid, int detailid, string detailname, byte? detailyesno, string detailcomment)
        {
            FacilityID = facilityid;
            DetailID = detailid;
            DetailName = detailname;

            if (detailyesno == null)
                DetailYesNo = 2;
            else
                DetailYesNo = detailyesno;

            DetailComment = detailcomment;
        }
    }
}