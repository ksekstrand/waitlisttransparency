﻿using Microsoft.AspNet.Identity;
using System;
using WTSite.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    public class SiteAdminInfo : AccountController
    {
        private WTEntities db = new WTEntities();

        public List<childcare_facility> PendingApprovalFacilities { get; set; }
        public List<int> EmailConfirmationPAFacilities { get; set; }
        public List<childcare_facility> OtherFacilities { get; set; }

        public SiteAdminInfo(List<childcare_facility> pendingapprovalfacilities, List<childcare_facility> otherfacilities)
        {
            PendingApprovalFacilities = pendingapprovalfacilities;

            EmailConfirmationPAFacilities = new List<int>();

            // Determines whether or not an account's email has been confirmed
            if (pendingapprovalfacilities.Count > 0)
            {
                foreach (childcare_facility facility in pendingapprovalfacilities)
                {
                    if (db.AspNetUsers.Where(item => item.UserName == facility.email_address).FirstOrDefault().EmailConfirmed)
                    {
                        EmailConfirmationPAFacilities.Add(1);
                    }
                    else
                    {
                        EmailConfirmationPAFacilities.Add(0);
                    }
                }
            }
            else
            {
                EmailConfirmationPAFacilities = new List<int>();
            }

            OtherFacilities = otherfacilities;
        }
    }
}