﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTSite.Models
{
    public class SearchResults
    {
        private WTEntities db = new WTEntities();

        public childcare_facility Facility { get; set; }
        public DateTime EED { get; set; }
        public List<PrioritizationEED> EEDsWithPrioritization { get; set; }
        public SearchCriteria SearchCriteria { get; set; }

        public SearchResults() { }

        public SearchResults(SearchCriteria searchcriteria, childcare_facility facility, DateTime eed, List<PrioritizationEED> eedswithprioritization = null)
        {                        
            SearchCriteria = searchcriteria;
            Facility = facility;
            EED = eed;
            EEDsWithPrioritization = eedswithprioritization;
        }
    }
}