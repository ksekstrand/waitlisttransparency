﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using WTSite.Helper_Classes;
using WTSite.Models;

namespace WTSite.DAL
{
    public class RoomDAL
    {
        private WTEntities db = new WTEntities();

        public int FacilityId { get; set; }
        public int? RoomId { get; set; }
        public List<room> FacilityRooms { get; set; }
        public List<ChildWaitlist> WaitlistChildren { get; set; }
        public List<ChildRoster> RosterChildren { get; set; }
        public List<prioritization> Prioritizations { get; set; }

        public RoomDAL(int facilityid)
        {
            FacilityId = facilityid;
            getFacilityRooms();
            // if the facility has rooms, make the first room the default room that displays
            // this might change depending on how rooms are selected on the front end
            if (FacilityRooms.Count > 0)
            {
                RoomId = FacilityRooms[0].room_id;
            }
        }

        public List<room> getFacilityRooms()
        {
            FacilityRooms = db.rooms.Where(item => item.childcare_facility_id == FacilityId).ToList();
            return FacilityRooms;
        }

        public room getRoomInformation()
        {
            room room = db.rooms.Find(RoomId);
            return room;
        }

        public List<prioritization> getFacilityPrioritizations()
        {
            Prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == FacilityId).OrderBy(item => item.priority_rank).ToList();
            return Prioritizations;
        }

        public List<ChildWaitlist> getWaitlistedChildren()
        {
            /*
            # The following query enumerates all children on a particular waitlist
            SELECT c.child_id, child_name, child_dob
            FROM child AS c
	            JOIN room AS r ON c.room_id = r.room_id
		            JOIN waitlist_child AS wc ON c.child_id = wc.child_id
            WHERE c.room_id = 1; #Sunshine
            */

            var query = from c in db.children
                        join r in db.rooms on c.room_id equals r.room_id
                        join wc in db.waitlist_child on c.child_id equals wc.child_id
                        where c.room_id == RoomId
                        select new
                        {
                            ChildId = c.child_id,
                            ChildName = c.child_name,
                            DOB = c.child_dob,
                            DateAdded = wc.date_added,
                            ChildComments = c.child_comment
                        };

            var tempWaitlist = query.ToList();
            
            // WaitlistChildren is a FacilityDetailsDAL class property
            WaitlistChildren = tempWaitlist.Select(item => new ChildWaitlist(
                item.ChildId, item.ChildName, item.DOB, item.DateAdded, new DateTime() ,item.ChildComments)).ToList();
            

            // sort WaitlistChildren based on highest prioritization rank, then by date added
            WaitlistChildren = WaitlistChildren.OrderBy(item => item.HighestPrioritizationRank).ThenBy(item => item.DateAdded).ToList();

            return WaitlistChildren;
        }

        public List<ChildRoster> getRosterChildren()
        {
            /*
            # The following query enumerates all children in a particular room a.k.a. as the room's roster
            SELECT DISTINCT c.child_id, child_name, child_dob, child_override_date
            FROM child AS c
            WHERE room_id = 1 AND child_id NOT IN (
	            SELECT c.child_id
                FROM child AS c
		            JOIN waitlist_child AS wc ON c.child_id = wc.child_id
	            WHERE c.room_id = 1
                ); #Sunshine
            */

            var query = (from c in db.children
                         where c.room_id == RoomId &&
                             !(from c2 in db.children
                               join wc in db.waitlist_child on c2.child_id equals wc.child_id
                               where c2.room_id == RoomId
                               select c2.child_id).Any(item => item == c.child_id)
                         select new
                         {
                             ChildId = c.child_id,
                             ChildName = c.child_name,
                             DOB = c.child_dob,
                             OverrideDate = c.child_override_date,
                             ChildComments = c.child_comment,
                             DateAdded = c.date_added,
                         }).Distinct();

            var tempRoster = query.ToList();

            // RosterChildren is a RoomsDAL class property
            RosterChildren = tempRoster.Select(item => new ChildRoster(
                item.ChildId, item.ChildName, item.DOB, (DateTime?)item.OverrideDate, item.DateAdded, item.ChildComments)).ToList();

            return RosterChildren;
        }

        /* 
        The following update, create and delete for children all use entity framework which I hope makes data binding easier
        and the views easier to create as well.
        */

        public void updateWaitlistChild(int childId, string childname, DateTime dob, DateTime? Date_added, String comments = "", PrioritizationVM childprioritizations = null)
        {
            // updates the child record  
            child child = db.children.Single(item => item.child_id == childId);

            child.child_name = childname;
            child.child_dob = dob;
            child.child_comment = comments;

            waitlist_child w = db.waitlist_child.First(item => child.child_id == item.child_id);
            w.date_added = Date_added;

            // remove current prioritizations
            db.Database.ExecuteSqlCommand(
                "DELETE FROM dbo.prioritization_child " +
                "WHERE child_id = " + child.child_id);
            db.SaveChanges();

            // if prioritizations is not null, add all the listed prioritizations to the child
            if (childprioritizations != null && childprioritizations.PrioritizationID != null)
            {
                // add new prioritizations to child
                for (int i = 0; i < childprioritizations.PrioritizationID.Length; i++)
                {
                    child.prioritizations.Add(db.prioritizations.Find(childprioritizations.PrioritizationID[i]));
                }
            }
            db.SaveChanges();
        }

        public void updateRosterChild(int childId, string childname, DateTime dob, DateTime? DateAdded, DateTime? overridedate = null, string comment = "")
        {
            // updates child record
            child child = db.children.Single(item => item.child_id == childId);

            child.child_name = childname;
            child.child_dob = dob;
            child.child_override_date = overridedate;
            child.child_comment = comment;
            child.date_added = DateAdded;

            db.SaveChanges();
        }

        public void createWaitlistChild(string childname, DateTime dob, PrioritizationVM childprioritizations = null)
        {
            // create the child record
            child child = new child
            {
                room_id = (int)RoomId,
                child_name = childname,
                child_dob = dob,
                date_added = DateTime.Now
            };
            db.children.Add(child);
            db.SaveChanges();

            // if prioritizations is not null, add all the listed prioritizations to the child
            if (childprioritizations != null && childprioritizations.PrioritizationID != null)
            {
                // add new prioritizations to child
                for (int i = 0; i < childprioritizations.PrioritizationID.Length; i++)
                {
                    child.prioritizations.Add(db.prioritizations.Find(childprioritizations.PrioritizationID[i]));
                }
            }
            db.SaveChanges();

            // create the waitlist_child record
            waitlist waitlist = db.waitlists.Single(item => item.room_id == RoomId);
            waitlist_child waitlistchild = new waitlist_child
            {
                waitlist_id = waitlist.waitlist_id,
                child_id = child.child_id,
                date_added = DateTime.Now
            };
            db.waitlist_child.Add(waitlistchild);
            db.SaveChanges();
        }

        public void createRosterChild(string childname, DateTime dob, DateTime? overridedate = null)
        {
            // create the child record
            child child = new child
            {
                room_id = (int)RoomId,
                child_name = childname,
                child_dob = dob,
                child_override_date = overridedate
            };

            db.children.Add(child);
            db.SaveChanges();
        }

        public void moveWaitlistToRosterChild(int childId)
        {
            // delete waitlist_child instance for the child
            waitlist_child waitlistchild = db.waitlist_child.Single(item => item.child_id == childId);

            db.waitlist_child.Remove(waitlistchild);
            db.SaveChanges();
        }

        public void deleteRosterChild(int childId)
        {
            // please note issues may arise because of the diamond multiple paths cascading errors issues in SQL SERVER
            child child = db.children.Single(item => item.child_id == childId);

            // remove current prioritizations
            db.Database.ExecuteSqlCommand(
                "DELETE FROM dbo.prioritization_child " +
                "WHERE child_id = " + child.child_id);

            db.children.Remove(child);
            db.SaveChanges();
        }

        public void deleteWaitlistChild(int childId)
        {
            // please note issues may arise because of the diamond multiple paths cascading errors issues in SQL SERVER
            child child = db.children.Single(item => item.child_id == childId);

            waitlist_child childWaitlist = db.waitlist_child.Where(item => item.child_id == child.child_id).FirstOrDefault();
            db.waitlist_child.Remove(childWaitlist);
            db.SaveChanges();

            // remove current prioritizations
            db.Database.ExecuteSqlCommand(
                "DELETE FROM dbo.prioritization_child " +
                "WHERE child_id = " + child.child_id);

            db.children.Remove(child);
            db.SaveChanges();
        }

        /* 
        The following update, create and delete for the room all use entity framework which I hope makes data binding easier
        and the views easier to create as well.
        */

        public void updateRoom(string roomname, int roomagemin, int roomagemax, int roomcapacity, DateTime? waitlistoverridedate)
        {
            room room = db.rooms.Single(item => item.room_id == RoomId);

            room.room_name = roomname;
            room.room_age_range_min = roomagemin;
            room.room_age_range_max = roomagemax;
            room.room_capacity = roomcapacity;
            room.waitlists.First().waitlist_override_date = waitlistoverridedate;

            db.SaveChanges();
        }

        public void createRoom(string roomname, int roomagemin, int roomagemax, int roomcapacity)
        {
            room room = new room
            {
                childcare_facility_id = FacilityId,
                room_name = roomname,
                room_age_range_min = roomagemin,
                room_age_range_max = roomagemax,
                room_capacity = roomcapacity
            };

            db.rooms.Add(room);
            db.SaveChanges();

            waitlist waitlist = new waitlist
            {
                room_id = room.room_id
            };

            db.waitlists.Add(waitlist);
            db.SaveChanges();

            // set current RoomId to the room that was just created
            RoomId = room.room_id;
        }

        public void deleteRoom()
        {
            // please note this is a hack because of the diamond cascading errors issues in SQL SERVER ):
            room room = db.rooms.Single(item => item.room_id == RoomId);

            WaitlistChildren = getWaitlistedChildren();
            if (WaitlistChildren.Count > 0)
            {
                foreach (ChildWaitlist child in WaitlistChildren)
                {
                    deleteWaitlistChild(child.ChildId);
                }
                db.SaveChanges();
            }

            RosterChildren = getRosterChildren();
            if (RosterChildren.Count > 0)
            {
                foreach (ChildRoster child in RosterChildren)
                {
                    deleteRosterChild(child.ChildId);
                }
                db.SaveChanges();
            }

            db.rooms.Remove(room);
            db.SaveChanges();

            // set the RoomId to null
            // RoomId will have to be set to another room_id to complete another action, or errors will be thrown
            RoomId = null;
        }
    }
}