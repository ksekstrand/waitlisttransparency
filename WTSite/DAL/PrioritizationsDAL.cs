﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTSite.Models;

namespace WTSite.DAL
{
    public class PrioritizationsDAL
    {
        private WTEntities db = new WTEntities();

        public int FacilityId { get; set; }

        public PrioritizationsDAL(int facilityId)
        {
            FacilityId = facilityId;
        }

        public List<prioritization> getPrioritizations()
        {
            List<prioritization> prioritizations = new List<prioritization>();

            prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == FacilityId).ToList();
            return prioritizations;
        }

        public void updatePrioritization(int priorityid, string priorityname, int priorityrank)
        {
            prioritization prioritization = db.prioritizations.Single(item => item.prioritization_id == priorityid);

            prioritization.priority_name = priorityname;
            prioritization.priority_rank = priorityrank;

            db.SaveChanges();
        }

        public void createPrioritization(string priorityname, int priorityrank)
        {
            prioritization prioritization = new prioritization
            {
                priority_name = priorityname,
                priority_rank = priorityrank,
                childcare_facility_id = FacilityId
            };

            db.prioritizations.Add(prioritization);
            db.SaveChanges();
        }

        public void deletePrioritization(int prioritizationid)
        {
            prioritization prioritization = db.prioritizations.Single(item => item.prioritization_id == prioritizationid);

            // remove current prioritizations
            db.Database.ExecuteSqlCommand(
                "DELETE FROM dbo.prioritization_child " +
                "WHERE prioritization_id = " + prioritizationid);

            db.prioritizations.Remove(prioritization);
            db.SaveChanges();
        }
    }
}