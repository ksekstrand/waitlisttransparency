﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTSite.Models;

namespace WTSite.DAL
{
    public class FacilityDetailsDAL
    {
        private WTEntities db = new WTEntities();

        public int FacilityId { get; set; }
        public List<FacilityDetail> Details { get; set; }

        public FacilityDetailsDAL(int facilityId)
        {
            FacilityId = facilityId;
            this.getFacilityDetails();
        }

        private void getFacilityDetails()
        {
            var query = from d in db.details
                        join cfd in
                            (
                                from fd in db.childcare_facility_detail
                                where fd.childcare_facility_id == FacilityId
                                select new
                                {
                                    DetailID = fd.detail_id,
                                    DetailYesNo = fd.detail_yes_no,
                                    DetailComment = fd.detail_comment
                                }
                            ) on d.detail_id equals cfd.DetailID into temp
                        from cfd in temp.DefaultIfEmpty()
                        select new
                        {
                            DetailID = d.detail_id,
                            DetailName = d.detail_name,
                            DetailYesNo = (byte?)cfd.DetailYesNo,
                            DetailComment = cfd.DetailComment ?? ""
                        };

            var tempDetails = query.ToList();

            // Details is a FacilityDetailsDALclass property
            Details = tempDetails.Select(item => new FacilityDetail(FacilityId,
                item.DetailID, item.DetailName, item.DetailYesNo, item.DetailComment)).ToList();
        }

        /* 
        The following update, create and delete all use entity framework which I hope makes data binding easier
        and the views easier to create as well.
        */

        public void updateFacilityDetail(int detailId, byte? detailYesNo, string detailComment)
        {
            childcare_facility_detail facilityDetail = db.childcare_facility_detail.Single(item => item.detail_id == detailId && item.childcare_facility_id == FacilityId);

            facilityDetail.detail_yes_no = (byte)detailYesNo;
            facilityDetail.detail_comment = detailComment;

            db.SaveChanges();
        }

        public void createFacilityDetail(int detailId, byte? detailYesNo, string detailComment)
        {
            childcare_facility_detail facilityDetail = new childcare_facility_detail
            {
                detail_id = detailId,
                childcare_facility_id = FacilityId,
                detail_yes_no = (byte)detailYesNo,
                detail_comment = detailComment
            };

            db.childcare_facility_detail.Add(facilityDetail);
            db.SaveChanges();
        }

        public void deleteFacilityDetail(int detailId)
        {
            childcare_facility_detail facilityDetail = db.childcare_facility_detail.Single(item => item.detail_id == detailId && item.childcare_facility_id == FacilityId);

            db.childcare_facility_detail.Remove(facilityDetail);
            db.SaveChanges();
        }
    }
}