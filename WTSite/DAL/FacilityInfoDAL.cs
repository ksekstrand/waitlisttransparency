﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTSite.Models;
using System.Text.RegularExpressions;

namespace WTSite.DAL
{
    public class FacilityInfoDAL
    {
        private WTEntities db = new WTEntities();

        public int FacilityId { get; set; }

        public FacilityInfoDAL(int facilityId)
        {
            FacilityId = facilityId;
        }

        public childcare_facility getFacilityInfo()
        {
            childcare_facility facility = db.childcare_facility.Find(FacilityId);
            return facility;
        }

        private static Regex digitsOnly = new Regex(@"[^\d]");

        public void updateFacilityInfo(string facilityName, string addressline1, string addressline2, string city, string state, string zipcode,
            string phonenumber, string website, string primarycontact, string licensenumber, DateTime licenseexpiration, int? isHidden)
        {
            childcare_facility facility = db.childcare_facility.Single(item => item.childcare_facility_id == FacilityId);
            //AspNetUser user = db.AspNetUsers.Single(item => item.UserName == facility.email_address);

            //bool identityExists = db.AspNetUsers.Any(m => m.UserName == emailaddress);

            //// BOMBY: this doesn't notify the user if the email is taken, it just will ignore the change.
            //// will probably have to use a ajax request to check if the email is taken and display the error 'real time'
            //if (!identityExists)
            //{
            //    user.UserName = emailaddress;
            //    user.Email = emailaddress;
            //}

            //if for some reason facility was saved as null, update to not hidden.
            if (facility.is_hidden != null)
                facility.is_hidden = isHidden;
            else
                facility.is_hidden = 0;
                
            facility.childcare_facility_name = facilityName;
            facility.address_line_1 = addressline1;
            facility.address_line_2 = addressline2;
            facility.city = city;
            facility.state = state;
            facility.zip_code = zipcode;
            facility.phone_number = ConvertToMaskedTelephone(phonenumber);
            //if (phonenumber != null)
            //{
            //    facility.phone_number = digitsOnly.Replace(phonenumber, "");
            //}
            //else
            //{
            //    facility.phone_number = null;
            //}
            
            //if (!identityExists)
            //{
            //    facility.email_address = emailaddress;  //if any identity user is found with the passed email, it will not be null. With null return email update is ok.
            //}            
            facility.website = TrimWebsite(website);
            facility.primary_contact = primarycontact;
            facility.childcare_facility_license_number = licensenumber;
            facility.childcare_facility_license_expiration = licenseexpiration;

            db.SaveChanges();
        }

        public void createNewFacility(string facilityName, string addressline1, string addressline2, string city, string state, string zipcode,
            string phonenumber, string emailaddress, string website, string primarycontact, string licensenumber, DateTime licenseexpiration)
        {
            childcare_facility newFacility = new childcare_facility();
            newFacility.childcare_facility_name = facilityName;
            newFacility.address_line_1 = addressline1;
            newFacility.address_line_2 = addressline2;
            newFacility.city = city;
            newFacility.state = state;
            newFacility.zip_code = zipcode;
            newFacility.phone_number = ConvertToMaskedTelephone(phonenumber);
            newFacility.email_address = emailaddress;
            newFacility.website = TrimWebsite(website);
            newFacility.primary_contact = primarycontact;
            newFacility.childcare_facility_license_number = licensenumber;
            newFacility.childcare_facility_license_expiration = licenseexpiration;
            newFacility.is_hidden = 0; //0 is not hidden

            db.childcare_facility.Add(newFacility);
            db.SaveChanges();
        }

        private string TrimWebsite(string website)
        {
            if (website == null)
            {
                return null;
            }

            else if ( website.Contains("http://") || website.Contains("https://") || website.Contains("HTTP://") || website.Contains("HTTPS://"))
            {
                System.Uri uri = new Uri(website);
                string trimmed = uri.Host + uri.PathAndQuery;
                return trimmed.TrimEnd('/');
            }

            else
            {
                return website.TrimEnd('/');
            }
        }

        private string ConvertToMaskedTelephone(string phoneNum)
        {
            if (phoneNum == null)
            {
                return null;
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneNum, @"^\d{10}$"))
            {
                return string.Format("{0:(###)###-####}", Int64.Parse(phoneNum));
            }
            else
            {
                return phoneNum;
            }
        }

    }
}