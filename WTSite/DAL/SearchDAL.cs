﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTSite.Helper_Classes;
using WTSite.Models;

namespace WTSite.DAL
{
    // this class needs further testing (and cleaning up maybe)
    public class SearchDAL
    {
        private WTEntities db = new WTEntities();
        private RoomDAL rm = null;

        public List<childcare_facility> Facilities { get; set; }
        public List<int?> FacilityRoom { get; set; }
        public AlgorithmChild CNC { get; set; }
        public List<DateTime> EED { get; set; }
        public List<List<PrioritizationEED>> EEDsWithPrioritization { get; set; }

        public SearchDAL() { }

        public List<SearchResults> getSearchFacilitiesByCityState(AlgorithmChild cnc, string city, string state)
        {
            // initialize variables
            EEDsWithPrioritization = new List<List<PrioritizationEED>>();
            FacilityRoom = new List<int?>();

            CNC = cnc; // set CNC Property to cnc

            // needs testing
            // account_activated of 1 means the facility is approved/unfrozen, 
            // which is the only status that should be displayed as a search result.
            Facilities = db.childcare_facility.Where(
                item => item.state == state && item.city == city && item.account_activated == 1 && item.is_hidden == 0 && item.rooms.Count > 0).ToList();

            // create SearchVM to be displayed
            List<SearchResults> searchResults = new List<SearchResults>();

            EED = new List<DateTime>();

            // if facilities is not null
            if (Facilities.Count > 0)
            {
                // call method to calculate each facilities EED, populate EED with these dates
                foreach (childcare_facility facility in Facilities)
                {
                    EED.Add(getEED(facility, CNC));
                }                

                for (var i = 0; i < Facilities.Count; i++)
                {
                    // to hold each prioritization
                    List<PrioritizationEED> prioritizationEEDS = new List<PrioritizationEED>();
                    int priorityRank = 0;
                    DateTime TempEED = new DateTime();

                    SearchDAL EEDCalculator = new SearchDAL();
                    rm = new RoomDAL(Facilities[i].childcare_facility_id);

                    List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();

                    // REVISIT FOR FURTHER TESTING
                    if (facilityPrioritizations.Count > 0)
                    {
                        if (FacilityRoom[i] != null)
                        {
                            foreach (prioritization prioritization in facilityPrioritizations)
                            {
                                priorityRank = prioritization.priority_rank;
                                TempEED = EEDCalculator.getEED(Facilities[i], (int)FacilityRoom[i], new AlgorithmChild(priorityRank));
                                prioritizationEEDS.Add(new PrioritizationEED(prioritization, TempEED));
                            }
                            EEDsWithPrioritization.Add(prioritizationEEDS);
                        }
                        else
                        {
                            EEDsWithPrioritization.Add(null);
                        }
                    }
                    else
                    {
                        EEDsWithPrioritization.Add(null);
                    }

                    // if the EED is not equal to 1/1/0001, do not include in result set, this is the default if there are no acceptible EEDs for a room
                    if (EED[i] != new DateTime())
                    {
                        searchResults.Add(new SearchResults(new SearchCriteria(CNC.DOB, state, city), Facilities[i], EED[i], EEDsWithPrioritization[i]));
                    }
                }
            }
            return searchResults;
        }

        // only finds facilities that match city and state [need other alternatives]
        public List<SearchResults> getSearchFacilitiesByZip(AlgorithmChild cnc, string zipcode)
        {
            // initialize variables
            EEDsWithPrioritization = new List<List<PrioritizationEED>>();
            FacilityRoom = new List<int?>();

            CNC = cnc; // set CNC Property to cnc

            // needs testing
            // account_activated of 1 means the facility is approved/unfrozen, 
            // which is the only status that should be displayed as a search result.
            Facilities = db.childcare_facility.Where(item => item.zip_code == zipcode.ToString() && item.account_activated == 1 && item.is_hidden == 0 && item.rooms.Count > 0).ToList();

            // create SearchVM to be displayed
            List<SearchResults> searchResults = new List<SearchResults>();

            EED = new List<DateTime>();

            // if facilities is not null
            if (Facilities.Count > 0)
            {
                // call method to calculate each facilities EED, populate EED with these dates
                foreach (childcare_facility facility in Facilities)
                {
                    EED.Add(getEED(facility, CNC));
                }

                for (var i = 0; i < Facilities.Count; i++)
                {
                    // to hold each prioritization
                    List<PrioritizationEED> prioritizationEEDS = new List<PrioritizationEED>();
                    int priorityRank = 0;
                    DateTime TempEED = new DateTime();

                    SearchDAL EEDCalculator = new SearchDAL();
                    rm = new RoomDAL(Facilities[i].childcare_facility_id);

                    List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();

                    // REVISIT FOR FURTHER TESTING
                    if (facilityPrioritizations.Count > 0)
                    {
                        if (FacilityRoom[i] != null)
                        {
                            foreach (prioritization prioritization in facilityPrioritizations)
                            {
                                priorityRank = prioritization.priority_rank;
                                TempEED = EEDCalculator.getEED(Facilities[i], (int)FacilityRoom[i], new AlgorithmChild(priorityRank));
                                prioritizationEEDS.Add(new PrioritizationEED(prioritization, TempEED));
                            }
                            EEDsWithPrioritization.Add(prioritizationEEDS);
                        }
                        else
                        {
                            EEDsWithPrioritization.Add(null);
                        }
                    }
                    else
                    {
                        EEDsWithPrioritization.Add(null);
                    }

                    // if the EED is not equal to 1/1/0001, do not include in result set, this is the default if there are no acceptible EEDs for a room
                    if (EED[i] != new DateTime())
                    {
                        searchResults.Add(new SearchResults(new SearchCriteria(CNC.DOB, zipcode), Facilities[i], EED[i], EEDsWithPrioritization[i]));
                    }
                }
            }
            return searchResults;
        }

        public DateTime getEED(childcare_facility facility, AlgorithmChild cnc)
        {
            int roomid = 0;
            DateTime returnDateTime = getEED(facility, roomid, cnc);

            return returnDateTime;
        }

        // use this method alone for the room page, use the roomid and the cnc parameters, send in a null facility
        public DateTime getEED(childcare_facility facility, int roomid, AlgorithmChild cnc)
        {
            // if using method for the room page, set CNC
            if (cnc != null)
            {
                CNC = cnc;
            }

            List<room> rooms = new List<room>();
            List<DateTime> eeds = new List<DateTime>();
            List<int> eedsrooms = new List<int>(); //delete one
            List<int> currentRoom = new List<int>(); // delete one

            // if using method for the room page, add room to rooms
            if (roomid != 0)
            {
                rooms.Add(db.rooms.Where(item => item.room_id == roomid).FirstOrDefault());
            }
            // else, get all of the facility's rooms
            else
            {
                rooms = facility.rooms.ToList();
            }

            // create RoomDAL to retrieve roster and waitlist foreach room
            RoomDAL getChildren;
            if (roomid != 0)
            {
                getChildren = new RoomDAL(rooms[0].childcare_facility_id);
            }
            else
            {
                getChildren = new RoomDAL(facility.childcare_facility_id);
            }

            if (rooms.Count > 0)
            {
                List<ChildRoster> rosterTemp = getChildren.getRosterChildren();
                List<ChildWaitlist> waitlistTemp = getChildren.getWaitlistedChildren();
                List<AlgorithmChild> roster = new List<AlgorithmChild>();
                List<AlgorithmChild> waitlist = new List<AlgorithmChild>();
                // foreach room, calculate the EED
                foreach (room room in rooms)
                {
                    // view each room to see if theres an override date, if so, store this as the EED for that room
                    if (room.waitlists.First().waitlist_override_date != null)
                    {
                        eeds.Add((DateTime)room.waitlists.First().waitlist_override_date);
                    }
                    // else if there is no override date, call method to calculate EED foreach room
                    else
                    {
                        getChildren.RoomId = room.room_id; // set RoomId to current room_id

                        // create and populate roster and waitlist with Child objects
                        rosterTemp = getChildren.getRosterChildren();
                        waitlistTemp = getChildren.getWaitlistedChildren();
                        roster = new List<AlgorithmChild>();
                        waitlist = new List<AlgorithmChild>();
                        // if the rosterTemp is not null, convert children to Child and add to roster
                        if (rosterTemp.Count > 0)
                        {
                            foreach (ChildRoster child in rosterTemp)
                            {
                                if (child.OverrideDate != null)
                                {
                                    roster.Add(new AlgorithmChild(child.ChildId, child.DOB, (DateTime)child.OverrideDate));
                                }
                                else
                                {
                                    roster.Add(new AlgorithmChild(child.ChildId, child.DOB));
                                }
                            }
                        }
                        // if the tempWaitlist is not null, convert children to Child and add to waitlist
                        if (waitlistTemp.Count > 0)
                        {
                            foreach (ChildWaitlist child in waitlistTemp)
                            {
                                // if the child has prioritizations, create a child with their highest, minimum, priority rank                         
                                if (child.Prioritizations.Count > 0)
                                {
                                    int prioritizationRank = (from item in child.Prioritizations
                                                              select item.priority_rank).Min();

                                    waitlist.Add(new AlgorithmChild(child.ChildId, child.DOB, prioritizationRank)); // add child to waitlist, to be passed to EEDAlgorithm
                                }
                                // else create a child without a priority rank
                                else
                                {
                                    waitlist.Add(new AlgorithmChild(child.ChildId, child.DOB)); // add child to waitlist, to be passed to EEDAlgorithm
                                }
                            }
                        }
                        // call method to calculate EED for each of the facilities rooms
                        EEDAlgorithm calculateEED = new EEDAlgorithm();
                        eeds.Add(calculateEED.calculateEstimatedEnrollmentDate(room.room_capacity, room.room_age_range_max, roster, waitlist, CNC));
                    }
                }
                // determine acceptible rooms based on the child's age upon enrollment
                List<DateTime> acceptibleEEDS = new List<DateTime>(); // this stores the DateTime of acceptible eeds

                // jerry rigged, used for retrieving EEDs with prioritizations for Search pages
                if (FacilityRoom == null)
                {
                    FacilityRoom = new List<int?>();
                }

                for (int i = 0; i < rooms.Count; i++)
                {
                    if (roomid != 0)
                    {
                        acceptibleEEDS.Add(eeds[i]);
                        eedsrooms.Add(rooms[0].room_id);
                    }
                    else
                    {
                        int testing = CNC.calculateMonthsOld(eeds[i]);
                        // if the child's age when enrolled is within the room's age range, add to acceptible
                        if (CNC.calculateMonthsOld(eeds[i]) >= rooms[i].room_age_range_min
                            && CNC.calculateMonthsOld(eeds[i]) <= rooms[i].room_age_range_max)
                        {
                            eedsrooms.Add(rooms[i].room_id); 

                            acceptibleEEDS.Add(eeds[i]);
                        }
                        // else, if the child is 2 months or under, place in room that accommodates babies (talk to sponsor about this area) 
                        else if (CNC.calculateMonthsOld(eeds[i]) <= 2)
                        {
                            // if the room matches the baby age range criteria, add as an acceptibleEED
                            if (rooms[i].room_age_range_min <= 2)
                            {
                                eedsrooms.Add(rooms[i].room_id);

                                acceptibleEEDS.Add(eeds[i]);
                            }
                            // else, add 1/1/0001 as the EED, which will be ignored in the getSearchFacilitiesBy...() method
                            else
                            {
                                eedsrooms.Add(rooms[i].room_id); 

                                acceptibleEEDS.Add(new DateTime());
                            }
                        }
                        // else, add 1/1/0001 as the EED, which will be ignored in the getSearchFacilitiesBy...() method
                        else
                        {
                            eedsrooms.Add(rooms[i].room_id);

                            acceptibleEEDS.Add(new DateTime());
                        }
                    }
                }
                // return the soonest acceptible EED
                // this is to filter out any new DateTime
                List<DateTime> tempEEDHolder = new List<DateTime>();
                List<int?> tempRoomsHolder = new List<int?>();
                for (var i = 0; i < acceptibleEEDS.Count; i++)
                {
                    if (acceptibleEEDS[i] != new DateTime())
                    {
                        tempEEDHolder.Add(acceptibleEEDS[i]);

                        tempRoomsHolder.Add(eedsrooms[i]);
                    }
                    else
                    {
                        tempRoomsHolder.Add(null);
                    }
                }
                // if there are no acceptible EEDs, they are all 1/1/0001, which throws an error if you try to get the min
                if (tempEEDHolder.Count <= 0)
                {
                    FacilityRoom.Add(null); // there is no acceptible EED, so the room_id does not really matter

                    return new DateTime();
                }
                // else, if there is only one item, the date at the first index (I think Min() will throw errors if there's only one, unsure)
                else if (tempEEDHolder.Count == 1)
                {
                    FacilityRoom.Add(tempRoomsHolder[0]);

                    return tempEEDHolder[0];
                }
                // else return the minimum date
                else
                {
                    FacilityRoom.Add(tempRoomsHolder[tempEEDHolder.IndexOf(tempEEDHolder.Min())]);

                    return tempEEDHolder.Min();
                }
            }
            // return today if the facility does not have any rooms, when they sign up and their account is activated, this is what will appear until they add rooms
            FacilityRoom.Add(null);

            return DateTime.Today;
        }
    }
}







