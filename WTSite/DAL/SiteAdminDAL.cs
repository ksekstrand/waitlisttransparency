﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WTSite.Models;

namespace WTSite.DAL
{
    public class SiteAdminDAL
    {
        private WTEntities db = new WTEntities();

        public SiteAdminDAL() { }

        public List<childcare_facility> getPendingApprovalFacilities()
        {
            List<childcare_facility> pendingApprovalFacilities = db.childcare_facility.Where(item => item.account_activated == 0).ToList();
            return pendingApprovalFacilities;
        }

        public List<childcare_facility> getUnconfirmedPendingApprovalFacilities()
        {
            // get all unconfirmed facilities
            List<AspNetUser> unconfirmedUsers = db.AspNetUsers.Where(item => item.EmailConfirmed == false).ToList();
            List<childcare_facility> unconfirmedPendingApprovalFacilities = new List<childcare_facility>();

            if (unconfirmedUsers.Count > 0)
            {
                foreach(AspNetUser user in unconfirmedUsers)
                {
                    childcare_facility facility = db.childcare_facility.Where(item => item.email_address == user.Email).FirstOrDefault();
                    // if facility is pending approval, which they should unless there is an error, add them to the unconfirmed pending approval facilities
                    if (facility != null && facility.account_activated == 0)
                    {
                        unconfirmedPendingApprovalFacilities.Add(facility);
                    }
                }
            }
            
            return unconfirmedPendingApprovalFacilities;
        }

        public List<childcare_facility> getOtherFacilities()
        {
            List<childcare_facility> otherfacilities = db.childcare_facility.Where(item => item.account_activated != 0 && item.account_activated != 4).ToList();
            return otherfacilities;
        }

        public List<childcare_facility> getArchivedFacilities()
        {
            List<childcare_facility> archivedFacilities = db.childcare_facility.Where(item => item.account_activated == 4).ToList();
            return archivedFacilities;
        }

        /*
        Notes about facility attribute, 'account_activated':
        0 - pending approval
        1 - approved/unfrozen
        2 - rejected
        3 - frozen
        4 - archived
        */

        public void approveFacility(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            AspNetUser user = db.AspNetUsers.Where(item => item.Email == facility.email_address).FirstOrDefault();

            if (!user.EmailConfirmed)
            {
                user.EmailConfirmed = true;
            }

            facility.account_activated = 1;
            db.SaveChanges();
        }

        public void rejectFacility(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            AspNetUser user = db.AspNetUsers.Where(item => item.Email == facility.email_address).FirstOrDefault();

            if (!user.EmailConfirmed)
            {
                user.EmailConfirmed = true;
            }

            facility.account_activated = 2;
            db.SaveChanges();
        }

        public void freezeFacility(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            facility.account_activated = 3;
            db.SaveChanges();
        }

        public void unfreezeFacility(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            facility.account_activated = 1;
            db.SaveChanges();
        }

        public void archiveFacility(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            AspNetUser user = db.AspNetUsers.Where(item => item.Email == facility.email_address).FirstOrDefault();
            
            if (!user.EmailConfirmed)
            {
                user.EmailConfirmed = true;
            }

            facility.account_activated = 4;
            db.SaveChanges();
        }

        public void deleteFacility(int id)
        {
            // delete all rooms in the facility, database issue workaround):
            RoomDAL rm = new RoomDAL(id);
            List<room> facilityRooms = rm.getFacilityRooms();
            if (facilityRooms.Count > 0)
            {
                foreach (room room in facilityRooms)
                {
                    rm.RoomId = room.room_id;
                    rm.deleteRoom();
                }
            }

            // delete facility
            childcare_facility facility = db.childcare_facility.Find(id);

            //for deleting identity records
            string facilityEmail = facility.email_address;
            db.childcare_facility.Remove(facility);

            db.SaveChanges();

            AspNetUser facilityIdentity = db.AspNetUsers.Where(item => item.UserName == facilityEmail).FirstOrDefault();
            db.AspNetUsers.Remove(facilityIdentity);

            db.SaveChanges();
        }

        public void updateDetailQuestion(int detailquestionid, string detailquestion)
        {
            detail detail = db.details.Single(item => item.detail_id == detailquestionid);

            detail.detail_name = detailquestion;

            db.SaveChanges();
        }

        public void createDetailQuestion(string detailquestion)
        {
            detail detail = new detail
            {
                detail_name = detailquestion
            };

            db.details.Add(detail);
            db.SaveChanges();
        }

        public void deleteDetailQuestion(int detailquestionid)
        {
            detail detail = db.details.Single(item => item.detail_id == detailquestionid);

            db.details.Remove(detail);
            db.SaveChanges();
        }

        public string getEmailAddress(int id)
        {
            childcare_facility facility = db.childcare_facility.Find(id);
            return facility.email_address;
        }
    }
}