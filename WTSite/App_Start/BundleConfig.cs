﻿using System.Web;
using System.Web.Optimization;

namespace WTSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(                      
                      "~/Content/font-awesome.min.css",
                       "~/Content/site.css",
                       "~/Content/sidebar.css"
                       ));
            
            bundles.Add(new StyleBundle("~/Content/HomepageStyle").Include(
                "~/Content/bootstrap.css",                
                "~/Content/jqueryui/jquery-ui.min.css"));


            bundles.Add(new ScriptBundle("~/bundles/HomepageScripts").Include(
                "~/Content/homepage/js/jquery.easing.min.js",
                "~/Content/homepage/js/jquery.fittext.js",
                "~/Content/homepage/js/cbpAnimatedHeader.js",                
                "~/Content/homepage/js/wow.min.js",
                "~/Scripts/jquery.maskedinput.min.js",
                "~/Scripts/jquery.validate.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/SupplementaryCss").Include(
                "~/Content/homepage/css/animate.min.css"
                //"~/Content/homepage/css/creative.css"
                ));


            //bundles.Add(new ScriptBundle("~/bundles/SearchPageScripts").Include(
            //    "~/Content/homepage/js/jquery.js",
            //    "~/Content/homepage/js/bootstrap.min.js",
            //    "~/Content/homepage/js/jquery.easing.min.js",
            //    "~/Content/homepage/js/jquery.fittext.js",
            //    "~/Content/homepage/js/creative.js"));
            BundleTable.EnableOptimizations = false;
        }
    }
}
