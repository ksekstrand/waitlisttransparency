﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WTSite.Startup))]
namespace WTSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
