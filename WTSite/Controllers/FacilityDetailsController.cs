﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WTSite.DAL;
using WTSite.Models;

namespace WTSite.Controllers
{
    [Authorize(Roles = "Facility, Admin")]
    public class FacilityDetailsController : Controller
    {
        private WTEntities db = new WTEntities();
        private FacilityDetailsDAL fd = null;

        [HttpGet]
        public ActionResult FacilityDetailsIndex() 
        {
            int id = (int)Session["CurrentFacilityId"];
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //int abcd = id.Value;
            fd = new FacilityDetailsDAL(id);

            return View(fd.Details);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult FacilityDetailsEdit(List<FacilityDetail> facilityDetails)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            if (facilityDetails[0].FacilityID != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                fd = new FacilityDetailsDAL(facilityDetails[0].FacilityID);

                for (int i = 0; i < facilityDetails.Count; i++)
                {
                    if (fd.Details[i].DetailYesNo == 2 && (facilityDetails[i].DetailYesNo == 1 || facilityDetails[i].DetailYesNo == 0))
                    {
                        fd.createFacilityDetail(facilityDetails[i].DetailID, facilityDetails[i].DetailYesNo, facilityDetails[i].DetailComment ?? "");
                    }
                    else if ((fd.Details[i].DetailYesNo == 1 || fd.Details[i].DetailYesNo == 0) && facilityDetails[i].DetailYesNo == 2)
                    {
                        fd.deleteFacilityDetail(facilityDetails[i].DetailID);
                    }
                    else if ((fd.Details[i].DetailYesNo == 1 || fd.Details[i].DetailYesNo == 0) && (facilityDetails[i].DetailYesNo == 1 || facilityDetails[i].DetailYesNo == 0))
                    {
                        fd.updateFacilityDetail(facilityDetails[i].DetailID, facilityDetails[i].DetailYesNo, facilityDetails[i].DetailComment ?? "");
                    }
                }
                return RedirectToAction("FacilityDetailsIndex");
            }       
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}