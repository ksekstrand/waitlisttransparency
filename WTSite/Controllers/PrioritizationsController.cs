﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WTSite.DAL;
using WTSite.Models;

namespace WTSite.Controllers
{
    [Authorize(Roles = "Facility, Admin")]
    public class PrioritizationsController : Controller
    {
        private WTEntities db = new WTEntities();
        private PrioritizationsDAL pz = null;

        [HttpGet]
        public ActionResult PrioritizationsIndex()
        {
            int id = (int)Session["CurrentFacilityId"];
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //int abcd = id.Value;

            List<prioritization> prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == id).OrderBy(item => item.priority_rank).ToList(); // check WT_Prototype if this doesn't work

            ViewBag.FacilityId = id;

            return View(prioritizations);            
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPrioritizations(List<prioritization> prioritizations)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                if (prioritizations == null || prioritizations.Count <= 0) // unsure which it will be, test this
                {
                    return RedirectToAction("PrioritizationsIndex");
                }
                // stops facilities from modifying other facility's data
                else if (prioritizations[0].childcare_facility_id != (int)Session["CurrentFacilityId"])
                {
                    return RedirectToAction("Error", "Home");
                }

                pz = new PrioritizationsDAL(prioritizations[0].childcare_facility_id);

                for (int i = 0; i < prioritizations.Count; i++)
                {
                    pz.updatePrioritization(prioritizations[i].prioritization_id, prioritizations[i].priority_name, prioritizations[i].priority_rank);
                }

                return RedirectToAction("PrioritizationsIndex");
            }
        }

        [ActionName("CreatePrioritization")]
        public ActionResult CreatePrioritization(int id)
        {
            // stops facilities from modifying other facility's data
            if (id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            List<prioritization> prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == id).OrderBy(item => item.priority_rank).ToList(); // check WT_Prototype if this doesn't work

            // if the facility has 15 prioritizations, do not allow them to create another
            // this is handled more elegantly client-side, this is just a safe-guard
            if (prioritizations.Count > 15)
            {
                return RedirectToAction("Error", "Home");
            }

            prioritizations.Add(new prioritization { childcare_facility_id = id });
            prioritizations[prioritizations.Count - 1].priority_rank = prioritizations.Count;

            ViewBag.FacilityId = id;

            return View(prioritizations);
        }

        [HttpPost]
        [ActionName("AddPrioritization")]
        public ActionResult AddPrioritization(List<prioritization> prioritizations)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // if the facility has 15 prioritizations, do not allow them to create another
            // this is handled more elegantly client-side, this is just a safe-guard
            else if (prioritizations.Count > 15)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (prioritizations[prioritizations.Count - 1].childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                pz = new PrioritizationsDAL(prioritizations[prioritizations.Count - 1].childcare_facility_id);

                pz.createPrioritization(prioritizations[prioritizations.Count - 1].priority_name, prioritizations[prioritizations.Count - 1].priority_rank);

                return RedirectToAction("PrioritizationsIndex");
            }
        }

        public ActionResult DeletePrioritization(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            prioritization prioritization = db.prioritizations.Find(id);

            if (prioritization == null)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (prioritization.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            int facilityId = prioritization.childcare_facility_id; // saving for the hack below

            pz = new PrioritizationsDAL(facilityId);

            pz.deletePrioritization(prioritization.prioritization_id);

            // this is pretty hacky, I'm going to re-rank each of the facility's prioritizations
            IList<prioritization> updatedPrioritizations = db.prioritizations.Where(item => item.childcare_facility_id == facilityId).OrderBy(item => item.priority_rank).ToList();

            if (updatedPrioritizations.Count > 0)
            {
                pz = new PrioritizationsDAL(facilityId);

                for (var i = 0; i < updatedPrioritizations.Count; i++)
                {
                    pz.updatePrioritization(updatedPrioritizations[i].prioritization_id, updatedPrioritizations[i].priority_name, (i + 1));
                }
            }

            return RedirectToAction("PrioritizationsIndex");
        }

        public ActionResult MovePrioritizationUp(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            prioritization prioritization = db.prioritizations.Find(id);

            if (prioritization == null)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (prioritization.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            int facilityId = prioritization.childcare_facility_id; // saving for the hack below

            List<prioritization> prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == facilityId).OrderBy(item => item.priority_rank).ToList();

            int currentPosition = prioritizations.IndexOf(prioritization); // index and also the rank that it will be changed to

            // if the prioritization is already at the top of the list, do nothing, redirect
            if (currentPosition == 0)
            {
                return RedirectToAction("PrioritizationsIndex");
            }

            // insert logic to move up
            pz = new PrioritizationsDAL(facilityId);

            pz.updatePrioritization(prioritizations[currentPosition].prioritization_id, prioritizations[currentPosition].priority_name, currentPosition);
            pz.updatePrioritization(prioritizations[currentPosition - 1].prioritization_id, prioritizations[currentPosition - 1].priority_name, currentPosition + 1);

            return RedirectToAction("PrioritizationsIndex");
        }

        public ActionResult MovePrioritizationDown(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            prioritization prioritization = db.prioritizations.Find(id);

            if (prioritization == null)
            {
                return HttpNotFound();
            }
            // stops facilities from modifying other facility's data
            else if (prioritization.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            int facilityId = prioritization.childcare_facility_id; // saving for the hack below

            List<prioritization> prioritizations = db.prioritizations.Where(item => item.childcare_facility_id == facilityId).OrderBy(item => item.priority_rank).ToList();

            int currentPosition = prioritizations.IndexOf(prioritization);

            // if the prioritization is already at the bottom of the list, do nothing, redirect
            if (currentPosition >= prioritizations.Count - 1)
            {
                return RedirectToAction("PrioritizationsIndex");
            }

            // insert logic to move down
            pz = new PrioritizationsDAL(facilityId);

            pz.updatePrioritization(prioritizations[currentPosition].prioritization_id, prioritizations[currentPosition].priority_name, currentPosition + 2);
            pz.updatePrioritization(prioritizations[currentPosition + 1].prioritization_id, prioritizations[currentPosition + 1].priority_name, currentPosition + 1);

            return RedirectToAction("PrioritizationsIndex");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}