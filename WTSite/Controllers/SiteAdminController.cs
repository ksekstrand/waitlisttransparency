﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WTSite.DAL;
using WTSite.Models;
using System.Net.Mail;

namespace WTSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SiteAdminController : Controller
    {
        private WTEntities db = new WTEntities();
        private SiteAdminDAL sa = null;

        [HttpGet]
        public ActionResult SiteAdminIndex()
        {
            List<childcare_facility> pendingApprovalFacilities = null;
            List<childcare_facility> otherFacilities = null;

            sa = new SiteAdminDAL();
            pendingApprovalFacilities = sa.getPendingApprovalFacilities();
            otherFacilities = sa.getOtherFacilities();

            SiteAdminInfo siteAdminInfo = new SiteAdminInfo(pendingApprovalFacilities, otherFacilities);

            Session["CurrentFacilityId"] = null;
            Session["CurrentFacilityName"] = null;

            return View(siteAdminInfo);
        }

        [ActionName("ArchivedFacilities")]
        public ActionResult ArchivedFacilities()
        {
            List<childcare_facility> archivedFacilities = null;

            sa = new SiteAdminDAL();
            archivedFacilities = sa.getArchivedFacilities();

            return View(archivedFacilities);
        }

        [ActionName("MaintainDetailQuestions")]
        public ActionResult MaintainDetailQuestions()
        {
            List<detail> details = db.details.ToList();

            return View(details);
        }

        [HttpPost]
        [ActionName("EditDetailQuestions")]
        [ValidateAntiForgeryToken]
        public ActionResult EditDetailQuestions(List<detail> details)
        {

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                sa = new SiteAdminDAL();

                for (int i = 0; i < details.Count; i++)
                {
                    sa.updateDetailQuestion(details[i].detail_id, details[i].detail_name);
                }
                return RedirectToAction("MaintainDetailQuestions");
            }                

        }

        [ActionName("CreateDetailQuestion")]
        public ActionResult CreateDetailQuestion()
        {
            List<detail> details = db.details.ToList();
            details.Add(new detail());

            return View(details);
        }

        [HttpPost]
        [ActionName("AddDetailQuestion")]
        public ActionResult AddDetailQuestion(List<detail> details)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                sa = new SiteAdminDAL();

                sa.createDetailQuestion(details[details.Count - 1].detail_name);

                return RedirectToAction("MaintainDetailQuestions");
            }
        }

        public ActionResult DeleteDetailQuestion(int id)
        {
            detail detail = db.details.Find(id);

            if (detail == null)
            {
                return HttpNotFound();
            }

            db.details.Remove(detail);
            db.SaveChanges();

            return RedirectToAction("MaintainDetailQuestions");
        }

        [ActionName("ApproveFacility")]
        public ActionResult ApproveFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.approveFacility(id);

            var callbackUrl = Url.Action("Login", "Account", null, protocol: Request.Url.Scheme);

            string emailAddr = sa.getEmailAddress(id);
            SmtpClient smtp = new SmtpClient();
            MailMessage sendApprovalEmail = new MailMessage("noreply@waitlisttransparency.com", emailAddr, "Waitlist Transparency - Facility Approved", "Your facility has been approved. Click <a href=\"" + callbackUrl + "\">here</a> to log in.");
            sendApprovalEmail.IsBodyHtml = true;
            smtp.Send(sendApprovalEmail);
            smtp.Dispose();

            return RedirectToAction("SiteAdminIndex");
        }

        [ActionName("RejectFacility")]
        public ActionResult RejectFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.rejectFacility(id);

            string emailAddr = sa.getEmailAddress(id);
            SmtpClient smtp = new SmtpClient();
            MailMessage sendRejectEmail = new MailMessage("noreply@waitlisttransparency.com", emailAddr, "Waitlist Transparency - Facility Rejected", "Your Facility Has Been Rejected");
            sendRejectEmail.IsBodyHtml = true;
            smtp.Send(sendRejectEmail);
            smtp.Dispose();

            return RedirectToAction("SiteAdminIndex");
            
        }

        [ActionName("FreezeFacility")]
        public ActionResult FreezeFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.freezeFacility(id);

            return RedirectToAction("SiteAdminIndex");
        }

        [ActionName("UnfreezeFacility")]
        public ActionResult UnfreezeFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.unfreezeFacility(id);

            return RedirectToAction("SiteAdminIndex");
        }

        [ActionName("ArchiveFacility")]
        public ActionResult ArchiveFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.archiveFacility(id);

            return RedirectToAction("SiteAdminIndex");
        }

        [ActionName("DeleteFacility")]
        public ActionResult DeleteFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.deleteFacility(id);

            return RedirectToAction("ArchivedFacilities");
        }

        [ActionName("DeleteUnconfirmedFacility")]
        public ActionResult DeleteUnconfirmedFacility(int id)
        {
            sa = new SiteAdminDAL();
            sa.deleteFacility(id);

            return RedirectToAction("SiteAdminIndex");
        }

        [ActionName("DeleteAllUnconfirmedFacilities")]
        public ActionResult DeleteAllUnconfirmedFacilities()
        {
            List<childcare_facility> unconfirmedPendingApprovalFacilities = null;

            sa = new SiteAdminDAL();
            unconfirmedPendingApprovalFacilities = sa.getUnconfirmedPendingApprovalFacilities();

            for (var i = 0; i < unconfirmedPendingApprovalFacilities.Count; i++)
            {
                // this if is a last safeguard to verify only pending approval facilities are being deleted
                if (unconfirmedPendingApprovalFacilities[i].account_activated == 0)
                {
                    sa.deleteFacility(unconfirmedPendingApprovalFacilities[i].childcare_facility_id);
                }
            }

            return RedirectToAction("SiteAdminIndex");
        }

        [HttpPost]
        [ActionName("DeleteAllArchivedFacilities")]
        public ActionResult DeleteAllArchivedFacilities(List<childcare_facility> archivedFacilities)
        {
            sa = new SiteAdminDAL();

            if (archivedFacilities != null)
            {
                for (var i = 0; i < archivedFacilities.Count; i++)
                {
                    // this if is a last safeguard to verify only archived facilities are being deleted
                    if (archivedFacilities[i].account_activated == 4)
                    {
                        sa.deleteFacility(archivedFacilities[i].childcare_facility_id);
                    }
                }
            }

            return RedirectToAction("ArchivedFacilities");
        }
    }
}