﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WTSite.Models;
using WTSite.DAL;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace WTSite.Controllers
{
    [Authorize(Roles = "Facility, Admin")]
    public class FacilityInfoController : Controller
    {
        private WTEntities db = new WTEntities();
        private FacilityInfoDAL fi = null;



        [HttpGet]
        public ActionResult FacilityInfoIndex(int? id=0)
        {
            Session["CurrentFacilityId"] = null;
            Session["CurrentFacilityName"] = null;

            childcare_facility facility = db.childcare_facility.Find(0); //temp id=0 initializer... replaced depending on if admin or facility. for some reason it cannot be declared as null so non-existing facility id 0 is used.
            if (User.IsInRole("Facility"))
            {
                facility = db.childcare_facility.Where(x => x.email_address == User.Identity.Name).FirstOrDefault(); //ignores any passed id, uses identity matching
            }
            else if (User.IsInRole("Admin") & Session["CurrentFacilityId"] != null)
            {
                facility = db.childcare_facility.Find((int)Session["CurrentFacilityId"]); //uses session id
            }
            else if (User.IsInRole("Admin") & Session["CurrentFacilityId"] == null)
            {
                facility = db.childcare_facility.Find(id); //uses id passed in
                Session["CurrentFacilityId"] = facility.childcare_facility_id;
                Session["CurrentFacilityName"] = facility.childcare_facility_name;
            }
            else
            {
                facility = null;
                return RedirectToAction("Error", "Home");
            }

            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //childcare_facility facility = db.childcare_facility.Find(id);
            if (facility == null)
            {
                return RedirectToAction("Error", "Home");
            }

            var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

            var myStatesArray = new SelectListItem[56];
            bool selectedState = false;

            for (var i = 0; i < statesArray.Length; i++)
            {
                if (facility.state == statesArray[i].ToString())
                {
                    selectedState = true;
                }
                else
                {
                    selectedState = false;
                }

                myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString(), Selected = selectedState };
            }

            ViewBag.myStatesArray = myStatesArray;

            // handles the admin permissions at the bottom of facility info for pending approval accounts
            AspNetUser facilityUser = db.AspNetUsers.Where(item => item.Email == facility.email_address).FirstOrDefault();
            ViewBag.EmailConfirmed = facilityUser.EmailConfirmed;

            Session["CurrentFacilityId"] = facility.childcare_facility_id;
            Session["CurrentFacilityName"] = facility.childcare_facility_name;

            //the view model does not exist for facility, so we need to assign a bool to the int value in the DB
            if (facility.is_hidden == 0)
                ViewBag.isFacilityHidden = false;
            else
                ViewBag.isFacilityHidden = true;

            return View(facility);
        }
        
        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult FacilityInfoEdit(childcare_facility facility)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            if (facility.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                
                fi = new FacilityInfoDAL(facility.childcare_facility_id);
                fi.updateFacilityInfo(facility.childcare_facility_name, facility.address_line_1, facility.address_line_2, facility.city, facility.state, facility.zip_code, facility.phone_number,
                    facility.website, facility.primary_contact, facility.childcare_facility_license_number, facility.childcare_facility_license_expiration, null);
                return RedirectToAction("FacilityInfoIndex","FacilityInfo",new { id = facility.childcare_facility_id });
            }
        }

        [HttpGet]
        [ActionName("FacilityInfoHide")]
       
        public ActionResult FacilityInfoHide(int id)
        {
            if (id != (int)Session["CurrentFacilityId"])
                return RedirectToAction("Error", "Home");

            childcare_facility facility = db.childcare_facility.FirstOrDefault(q => q.childcare_facility_id == id);

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            if (facility.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            if (facility.is_hidden == 1)
                facility.is_hidden = 0;
            else
                facility.is_hidden = 1;


            fi = new FacilityInfoDAL(facility.childcare_facility_id);
            fi.updateFacilityInfo(facility.childcare_facility_name, facility.address_line_1, facility.address_line_2, facility.city, facility.state, facility.zip_code, facility.phone_number,
                facility.website, facility.primary_contact, facility.childcare_facility_license_number, facility.childcare_facility_license_expiration, facility.is_hidden);

            return RedirectToAction("FacilityInfoIndex", "FacilityInfo", new { id = facility.childcare_facility_id });
        }


        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            return View();
        }

    }
}