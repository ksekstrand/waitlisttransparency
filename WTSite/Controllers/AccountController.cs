﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WTSite.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using WTSite.DAL;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using reCAPTCHA.MVC;

namespace WTSite.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private WTEntities db = new WTEntities();
        private FacilityDetailsDAL fd = null;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        //[RequireHttps] since SSL is required this will only work in deployed testing. Needs to be added before final deployment. 
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!db.AspNetUsers.Any(item => item.Email == model.Email))
            {
                ModelState.AddModelError("", "Invalid login attempt.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            //if (User.IsInRole("Admin"))
            //if (model.Email == "admin@test.com") //NEED TO CHANGE TO HANDLE ADMIN USER BETTER, THIS IS TEMPORARY
            //{
            //    return RedirectToAction("SiteAdminIndex", "SiteAdmin");
            //}

            //var userId = UserManager.FindByEmail(model.Email).Id;
            //if(UserManager.IsInRole(userId,"Admin"))
            //{
            //    return RedirectToAction("SiteAdminIndex", "SiteAdmin");
            //}

            //verify admin role
            //string currentAspNetUserId = db.AspNetUsers.Where(m => m.Email == model.Email).FirstOrDefault().Id;
            //AspNetRole currentRole = db.AspNetRoles.Where(m => m.Id == currentAspNetUserId).FirstOrDefault();
            //if(currentRole.Id == "Admin")
            //{
            //    return RedirectToAction("SiteAdminIndex", "SiteAdmin");
            //}

            childcare_facility findFacility;
            findFacility = db.childcare_facility.Where(m => m.email_address == model.Email).FirstOrDefault();
            if (findFacility == null) //redirect to admin page if no facilities found. it is assumed accounts with no facility attached are admin accounts.
            {                         //if somehow an identity record without a facility gets generated it will still direct to siteadmin but return an error because of no Admin role.
                return RedirectToAction("SiteAdminIndex", "SiteAdmin");
            }
            int accountStatus = findFacility.account_activated;

            
            /*
            Notes about facility attribute, 'account_activated':
            0 - pending approval
            1 - approved/unfrozen
            2 - rejected
            3 - frozen
            4 - archived
            */
            switch (result)
            {
                case SignInStatus.Success:                              //nested switch statement -> after identity signin success, redirect based on facility status
                    {                                                   //original: return RedirectToLocal(model.ReturnUrl);
                                                                        //retrieve logged in user's account status
                        switch (accountStatus)
                        {
                            case 0:
                                {
                                    ViewBag.ReasonDenied = "Your facility is pending approval.";
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return View("AccessDenied");
                                }
                            case 1:     
                                    return RedirectToAction("FacilityInfoIndex", "FacilityInfo"); 
                            case 2:
                                {
                                    ViewBag.ReasonDenied = "Your facility registration was not approved.";
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return View("AccessDenied");
                                }
                            case 3:
                                {
                                    ViewBag.ReasonDenied = "Your account is frozen.";
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return View("AccessDenied");
                                }
                            case 4:
                                {
                                    ViewBag.ReasonDenied = "Your account was archived or deleted.";
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return View("AccessDenied");
                                }
                            default:
                                {
                                    ViewBag.ReasonDenied = "ERROR";
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return View("AccessDenied");
                                }
                        }
                      
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        public ActionResult RedirectByRole()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("SiteAdminIndex", "SiteAdmin");
            }
            else
            {
                return RedirectToAction("SearchIndex", "Search");
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }



            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

            var myStatesArray = new SelectListItem[56];

            for (var i = 0; i < statesArray.Length; i++)
            {
                myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString() };
            }

            ViewBag.myStatesArray = myStatesArray;

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [CaptchaValidator]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {

            var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

            var myStatesArray = new SelectListItem[56];

            for (var i = 0; i < statesArray.Length; i++)
            {
                myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString() };
            }

            ViewBag.myStatesArray = myStatesArray;

            using (var context = new ApplicationDbContext())
            {
                if (model.Email.ToLower() != model.ConfirmEmail.ToLower())
                {
                    ModelState.AddModelError("Email", "Please confirm your email address.");
                }
                if (ModelState.IsValid)
                {
                    try
                    {
                        var user = new ApplicationUser { UserName = model.Email.ToLower(), Email = model.Email.ToLower() };
                        var result = await UserManager.CreateAsync(user, model.Password);

                        if (result.Succeeded)
                        {
                            //send email confirmation code
                            string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                            await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                            //Set role to facility
                            UserManager.AddToRole(user.Id, "Facility");

                            ////
                            //CREATE NEW FACILITY HERE
                            ////
                            childcare_facility newFacility = new childcare_facility
                            {
                                childcare_facility_name = model.FacilityModel.childcare_facility_name,
                                address_line_1 = model.FacilityModel.address_line_1,
                                address_line_2 = model.FacilityModel.address_line_2,
                                city = model.FacilityModel.city,
                                state = model.FacilityModel.state,
                                zip_code = model.FacilityModel.zip_code,
                                phone_number = ConvertToMaskedTelephone(model.FacilityModel.phone_number),
                                email_address = model.Email.ToLower(), //using same email input as Identity login email to prevent confusion
                                user_id = user.Id,
                                website = TrimWebsite(model.FacilityModel.website),
                                primary_contact = model.FacilityModel.primary_contact,
                                childcare_facility_license_number = model.FacilityModel.childcare_facility_license_number,
                                childcare_facility_license_expiration = model.FacilityModel.childcare_facility_license_expiration
                            };

                            db.childcare_facility.Add(newFacility);
                            db.SaveChanges();

                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                            // Send an email with this link

                            //retrieve facilityId of newly generated facility and pass id to RegisterDetails controller
                            //int newId = xxxxxxxxxx; 

                            //newFacility = db.childcare_facility.LastOrDefault(); //reload from db to retrieve db-generated id number. may cause errors
                            //in simultaneous registration. maybe matching by email would be better.
                            newFacility = db.childcare_facility.Where(m => m.email_address == model.Email).Single();
                            int newId = newFacility.childcare_facility_id;

                            Session["NewId"] = newId;

                            return RedirectToAction("RegisterDetails");
                        
                        }
                        AddErrors(result);
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                    ve.PropertyName,
                                    eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                    ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                }
                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        private string TrimWebsite(string website)
        {
            if (website == null)
            {
                return null;
            }

            else if (website.Contains("http://") || website.Contains("https://") || website.Contains("HTTP://") || website.Contains("HTTPS://"))
            {
                System.Uri uri = new Uri(website);
                string trimmed = uri.Host + uri.PathAndQuery;
                return trimmed.TrimEnd('/');
            }

            else
            {
                return website.TrimEnd('/');
            }
        }

        private string ConvertToMaskedTelephone(string phoneNum)
        {
            if (phoneNum == null)
            {
                return null;
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneNum, @"^\d{10}$"))
            {
                return String.Format("{0:(###)###-####}", phoneNum);
            }
            else
            {
                return phoneNum;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RegisterDetails() //(RegisterDetailsViewModel model)
        {
            int id = (int)Session["NewId"];
            fd = new FacilityDetailsDAL(id);
            return View(fd.Details);
        }

        [HttpPost]
        [ActionName("EditDetails")]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult FacilityDetailsEdit(List<FacilityDetail> facilityDetails)
        {
            if (ModelState.IsValid)
            {
                fd = new FacilityDetailsDAL(facilityDetails[0].FacilityID);

                for (int i = 0; i < facilityDetails.Count; i++)
                {
                    if (fd.Details[i].DetailYesNo == 2 && (facilityDetails[i].DetailYesNo == 1 || facilityDetails[i].DetailYesNo == 0))
                    {
                        fd.createFacilityDetail(facilityDetails[i].DetailID, facilityDetails[i].DetailYesNo, facilityDetails[i].DetailComment ?? "");
                    }
                    else if ((fd.Details[i].DetailYesNo == 1 || fd.Details[i].DetailYesNo == 0) && facilityDetails[i].DetailYesNo == 2)
                    {
                        fd.deleteFacilityDetail(facilityDetails[i].DetailID);
                    }
                    else if ((fd.Details[i].DetailYesNo == 1 || fd.Details[i].DetailYesNo == 0) && (facilityDetails[i].DetailYesNo == 1 || facilityDetails[i].DetailYesNo == 0))
                    {
                        fd.updateFacilityDetail(facilityDetails[i].DetailID, facilityDetails[i].DetailYesNo, facilityDetails[i].DetailComment ?? "");
                    }
                }
                return RedirectToAction("RegisterComplete");
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult RegisterComplete()
        {
            return View();
        }

        //private ActionResult CreateFacility(childcare_facility newFacility)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.childcare_facility.Add(newFacility);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        return View(newFacility);
        //    }
        //}


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            

            if (result.Succeeded)
            {
                //await UserManager.SendEmailAsync(userId, "New User Registered", "A new user has confirmed their account. Please approve or deny.");
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.Subject = "New Facility Added on Waitlist Transparency" ;
                mailMessage.Body = "There has been a new user registered on Waitlisttransparency.com. Please approve or reject.";
                mailMessage.To.Add("Info@waitlisttransparency.com");

                System.Net.Mail.SmtpClient oSMTPClient = new System.Net.Mail.SmtpClient();
                oSMTPClient.Send(mailMessage);
            }
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }



        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}