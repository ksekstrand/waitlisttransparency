﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WTSite.DAL;
using WTSite.Helper_Classes;
using WTSite.Models;

namespace WTSite.Controllers
{
    [Authorize(Roles = "Facility, Admin")]
    public class RoomsController : Controller
    {
        private WTEntities db = new WTEntities();
        private RoomDAL rm = null;

        [HttpGet]
        public ActionResult RoomsIndex(int? id = 0)
        {
            
            if (Session["CurrentFacilityId"] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            id = (int)Session["CurrentFacilityId"];
            int abcd = id.Value;

            List<room> rooms = db.rooms.Where(item => item.childcare_facility_id == id).ToList();

            ViewBag.FacilityId = abcd;

            return View(rooms);
        }

        [ActionName("CreateRoom")]
        public ActionResult CreateRoom(int id)
        {
            // stops facilities from modifying other facility's data
            if (id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            List<room> rooms = db.rooms.Where(item => item.childcare_facility_id == id).ToList();

            rooms.Add(new room { childcare_facility_id = id });

            ViewBag.FacilityId = id;

            return View(rooms);
        }

        [HttpPost]
        [ActionName("AddRoom")]
        public ActionResult AddRoom(List<room> rooms)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                // stops facilities from modifying other facility's data
                if (rooms[rooms.Count - 1].childcare_facility_id != (int)Session["CurrentFacilityId"])
                {
                    return RedirectToAction("Error", "Home");
                }

                // if room age min > room age max, send to error page (server-side safe-guard)
                if (rooms[rooms.Count - 1].room_age_range_min > rooms[rooms.Count - 1].room_age_range_max)
                {
                    return RedirectToAction("Error", "Home");
                }

                rm = new RoomDAL(rooms[rooms.Count - 1].childcare_facility_id);

                rm.createRoom(rooms[rooms.Count - 1].room_name, rooms[rooms.Count - 1].room_age_range_min, rooms[rooms.Count - 1].room_age_range_max, rooms[rooms.Count - 1].room_capacity);

                return RedirectToAction("RoomsIndex");
            }
        }

        public ActionResult DeleteRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);

            if (room == null)
            {
                return HttpNotFound();
            }
            // stops facilities from modifying other facility's data
            else if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);
            rm.RoomId = room.room_id;
            rm.deleteRoom();

            return RedirectToAction("RoomsIndex");
        }

        [ActionName("EditRoom")]
        public ActionResult EditRoom(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            room room = db.rooms.Find(id);

            if (Session["CurrentFacilityId"] == null)
            {
                return RedirectToAction("Error", "Home");
            }

            // stops facilities from modifying other facility's data
            if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);
            rm.RoomId = id;

            List<ChildRoster> rosterChildren = rm.getRosterChildren();

            List<ChildWaitlist> waitlistChildren = rm.getWaitlistedChildren();

            List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();

            childcare_facility facility = db.childcare_facility.Find(room.childcare_facility_id);

            SearchDAL EEDCalculator = new SearchDAL();
            DateTime EEDWithoutPrioritization = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild());

            // to hold each prioritization
            List<PrioritizationEED> prioritizationEEDS = new List<PrioritizationEED>();
            int priorityRank = 0;
            DateTime EED = new DateTime();

            if (facilityPrioritizations.Count > 0)
            {
                foreach (prioritization prioritization in facilityPrioritizations)
                {
                    priorityRank = prioritization.priority_rank;
                    EED = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild(priorityRank));
                    prioritizationEEDS.Add(new PrioritizationEED(prioritization, EED));
                }
            }
            RoomVM roomInfo = new RoomVM(room, room.waitlists.First().waitlist_override_date, rosterChildren, waitlistChildren, facilityPrioritizations, EEDWithoutPrioritization, prioritizationEEDS);

            List<PrioritizationVM> childrenPrioritizationVMs = new List<PrioritizationVM>();

            List<int> selectedIds = new List<int>();
            for (var i = 0; i < waitlistChildren.Count; i++)
            {
                selectedIds = new List<int>();
                var myFacilityPrioritizations = new SelectListItem[facilityPrioritizations.Count];

                for (var j = 0; j < myFacilityPrioritizations.Length; j++)
                {
                    prioritization thepr = waitlistChildren[i].Prioritizations.FirstOrDefault(item => item.prioritization_id == facilityPrioritizations[j].prioritization_id);

                    if (!(thepr == null))
                        selectedIds.Add(thepr.prioritization_id);
                }
                childrenPrioritizationVMs.Add(new PrioritizationVM()
                {
                    PrioritizationID = selectedIds.ToArray(),
                    Prioritizations = new MultiSelectList(facilityPrioritizations, "prioritization_id", "priority_name", selectedIds),
                    ChildId = waitlistChildren[i].ChildId
                });
            }


            EEDAlgorithm calculateEED = new EEDAlgorithm();

            var templistwait = new List<AlgorithmChild>();


            // if the tempWaitlist is not null, convert children to Child and add to waitlist
            if (waitlistChildren.Count > 0)
            {
                foreach (ChildWaitlist child in waitlistChildren)
                {
                    // if the child has prioritizations, create a child with their highest, minimum, priority rank                         
                    if (child.Prioritizations.Count > 0)
                    {
                        int prioritizationRank = (from item in child.Prioritizations
                                                  select item.priority_rank).Min();

                        templistwait.Add(new AlgorithmChild(child.ChildId, child.DOB, prioritizationRank)); // add child to waitlist, to be passed to EEDAlgorithm
                    }
                    // else create a child without a priority rank
                    else
                    {
                        templistwait.Add(new AlgorithmChild(child.ChildId, child.DOB)); // add child to waitlist, to be passed to EEDAlgorithm
                    }
                }
            }

            getAllEEDs(room, rosterChildren, waitlistChildren, calculateEED);

            roomInfo.ChildrenPrioritizationVMs = childrenPrioritizationVMs;
            return View(roomInfo);
        }

        private static void getAllEEDs(room room, List<ChildRoster> rosterChildren, List<ChildWaitlist> waitlistChildren, EEDAlgorithm calculateEED)
        {
            List<AlgorithmChild> templistrost = new List<AlgorithmChild>();
            for (int i = 0; i < waitlistChildren.Count; i++)
            {
                templistrost = new List<AlgorithmChild>();

                DateTime d = new DateTime();
                AlgorithmChild alg = new AlgorithmChild();
                List<AlgorithmChild> tempalgwait = new List<AlgorithmChild>();
                alg.ChildID = waitlistChildren[i].ChildId;
                alg.DOB = waitlistChildren[i].DOB;
                alg.PrioritizationRank = waitlistChildren[i].HighestPrioritizationRank;

                if (rosterChildren.Count > 0)
                {
                    foreach (ChildRoster child in rosterChildren)
                    {
                        if (child.OverrideDate != null)
                        {
                            templistrost.Add(new AlgorithmChild(child.ChildId, child.DOB, (DateTime)child.OverrideDate));
                        }
                        else
                        {
                            templistrost.Add(new AlgorithmChild(child.ChildId, child.DOB));
                        }
                    }
                }

                for (int j = 0; j < (i + 1) - j; j++)
                {
                    if (i != 0)
                    {
                        if (waitlistChildren[j].Prioritizations.Count > 0)
                        {
                            int prioritizationRank = (from item in waitlistChildren[j].Prioritizations
                                                      select item.priority_rank).Min();

                            tempalgwait.Add(new AlgorithmChild(waitlistChildren[j].ChildId, waitlistChildren[j].DOB, prioritizationRank)); // add child to waitlist, to be passed to EEDAlgorithm
                        }
                        // else create a child without a priority rank
                        else
                        {
                            tempalgwait.Add(new AlgorithmChild(waitlistChildren[j].ChildId, waitlistChildren[j].DOB)); // add child to waitlist, to be passed to EEDAlgorithm
                        }
                    }
                    else
                    {

                    }
                }

                d = calculateEED.calculateEstimatedEnrollmentDate(room.room_capacity, room.room_age_range_max, templistrost, tempalgwait, alg);
                waitlistChildren[i].EED = d;
            }
        }

        public ActionResult Export(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            room room = db.rooms.Find(id);

            // stops facilities from exporting other facility's data
            if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);
            rm.RoomId = id;

            List<ChildRoster> rosterChildren = rm.getRosterChildren();

            List<ChildWaitlist> waitlistChildren = rm.getWaitlistedChildren();

            List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();

            childcare_facility facility = db.childcare_facility.Find(room.childcare_facility_id);

            SearchDAL EEDCalculator = new SearchDAL();
            DateTime EEDWithoutPrioritization = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild());

            // to hold each prioritization
            List<PrioritizationEED> prioritizationEEDS = new List<PrioritizationEED>();
            int priorityRank = 0;
            DateTime EED = new DateTime();

            if (facilityPrioritizations.Count > 0)
            {
                foreach (prioritization prioritization in facilityPrioritizations)
                {
                    priorityRank = prioritization.priority_rank;
                    EED = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild(priorityRank));
                    prioritizationEEDS.Add(new PrioritizationEED(prioritization, EED));
                }
            }

            if(waitlistChildren.Count > 0)
            {
                foreach (ChildWaitlist c in waitlistChildren)
                {
                    c.EED = EEDCalculator.getEED(facility,room.room_id, new AlgorithmChild(c.ChildId, c.DOB, c.HighestPrioritizationRank));
                }
            }

            RoomVM roomInfo = new RoomVM(room, room.waitlists.First().waitlist_override_date, rosterChildren, waitlistChildren, facilityPrioritizations, EEDWithoutPrioritization, prioritizationEEDS);

            List<PrioritizationVM> childrenPrioritizationVMs = new List<PrioritizationVM>();

            List<int> selectedIds = new List<int>();
            for (var i = 0; i < waitlistChildren.Count; i++)
            {
                selectedIds = new List<int>();
                var myFacilityPrioritizations = new SelectListItem[facilityPrioritizations.Count];

                for (var j = 0; j < myFacilityPrioritizations.Length; j++)
                {
                    prioritization thepr = waitlistChildren[i].Prioritizations.FirstOrDefault(item => item.prioritization_id == facilityPrioritizations[j].prioritization_id);

                    if (!(thepr == null))
                        selectedIds.Add(thepr.prioritization_id);
                }
                childrenPrioritizationVMs.Add(new PrioritizationVM()
                {
                    PrioritizationID = selectedIds.ToArray(),
                    Prioritizations = new MultiSelectList(facilityPrioritizations, "prioritization_id", "priority_name", selectedIds),
                    ChildId = waitlistChildren[i].ChildId
                });
            }

            getAllEEDs(room, rosterChildren, waitlistChildren, new EEDAlgorithm());

            roomInfo.ChildrenPrioritizationVMs = childrenPrioritizationVMs;

            // code that exports the "Export" view into an excel document
            Response.AddHeader("content-disposition", "attachment;filename=" + facility.childcare_facility_name + "_" + roomInfo.Room.room_name + ".xls");
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");

            return View(roomInfo);
        }

        [HttpPost]
        public ActionResult EditRoomInfo(RoomVM roominfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (roominfo.Room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                // if room age min > room age max, send to error page (server-side safe-guard)
                if (roominfo.Room.room_age_range_min > roominfo.Room.room_age_range_max)
                {
                    return RedirectToAction("Error", "Home");
                }

                rm = new RoomDAL(roominfo.Room.childcare_facility_id);
                rm.RoomId = roominfo.Room.room_id;

                rm.updateRoom(roominfo.Room.room_name, roominfo.Room.room_age_range_min, roominfo.Room.room_age_range_max, 
                    roominfo.Room.room_capacity, roominfo.WaitlistOverrideDate);

                return RedirectToAction("EditRoom", new { id = roominfo.Room.room_id });
            }
        }

        [HttpPost]
        public ActionResult EditRoomRoster(RoomVM rosterinfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (rosterinfo.Room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                rm = new RoomDAL(rosterinfo.Room.childcare_facility_id);
                rm.RoomId = rosterinfo.Room.room_id;

                for (var i = 0; i < rosterinfo.RosterChildren.Count; i++)
                {
                    rm.updateRosterChild(rosterinfo.RosterChildren[i].ChildId, rosterinfo.RosterChildren[i].ChildName, rosterinfo.RosterChildren[i].DOB, rosterinfo.RosterChildren[i].DateAdded, rosterinfo.RosterChildren[i].OverrideDate, rosterinfo.RosterChildren[i].ChildComments);
                }

                return RedirectToAction("EditRoom", new { id = rosterinfo.Room.room_id });
            }
        }

        [ActionName("CreateRosterChild")]
        public ActionResult CreateRosterChild(int? id)
        {
            // I don't know if we need this but whatever
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                room room = db.rooms.Find(id);

                // stops facilities from modifying other facility's data
                if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
                {
                    return RedirectToAction("Error", "Home");
                }

                rm = new RoomDAL(room.childcare_facility_id);
                rm.RoomId = id;

                List<ChildRoster> rosterChildren = rm.getRosterChildren();
                rosterChildren.Add(new ChildRoster()); // add empty item to hold new roster child information

                List<ChildWaitlist> waitlistChildren = rm.getWaitlistedChildren();

                childcare_facility facility = db.childcare_facility.Find(room.childcare_facility_id);

                SearchDAL EEDCalculator = new SearchDAL();
                DateTime EEDWithoutPrioritization = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild());

                // to collect the facilities prioritizations
                List<prioritization> facilityPrioritizations = db.prioritizations.Where(item => item.childcare_facility_id == room.childcare_facility_id).ToList();
                // to hold each prioritization
                List<PrioritizationEED> prioritizationEEDS = null;

                if (facilityPrioritizations.Count <= 0)
                {
                    foreach (prioritization prioritization in facilityPrioritizations)
                    {
                        prioritizationEEDS.Add(new PrioritizationEED(prioritization, EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild(prioritization.priority_rank))));
                    }
                }

                RoomVM roomInfo = new RoomVM(room, room.waitlists.First().waitlist_override_date, rosterChildren, waitlistChildren, facilityPrioritizations, EEDWithoutPrioritization, prioritizationEEDS);

                ViewBag.RoomId = id;

                return View(roomInfo);
            }
        }

        [HttpPost]
        [ActionName("AddRosterChild")]
        public ActionResult AddRosterChild(RoomVM rosterinfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (rosterinfo.Room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                rm = new RoomDAL(rosterinfo.Room.childcare_facility_id);
                rm.RoomId = rosterinfo.Room.room_id;

                rm.createRosterChild(rosterinfo.RosterChildren[rosterinfo.RosterChildren.Count - 1].ChildName, rosterinfo.RosterChildren[rosterinfo.RosterChildren.Count - 1].DOB,
                    rosterinfo.RosterChildren[rosterinfo.RosterChildren.Count - 1].OverrideDate);

                return RedirectToAction("EditRoom", new { id = rosterinfo.Room.room_id });
            }
        }

        public ActionResult DeleteRosterChild(int? id)
        {
            // don't really need this?
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            child child = db.children.Find(id);

            int roomId = child.room_id;

            room room = db.rooms.Find(roomId);

            if (child == null)
            {
                return HttpNotFound();
            }
            // stops facilities from modifying other facility's data
            else if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);

            rm.deleteRosterChild(child.child_id);

            return RedirectToAction("EditRoom", new { id = roomId });
        }

        [HttpPost]
        public ActionResult EditRoomWaitlist(RoomVM waitlistinfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (waitlistinfo.Room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                rm = new RoomDAL(waitlistinfo.Room.childcare_facility_id);
                rm.RoomId = waitlistinfo.Room.room_id;

                for (var i = 0; i < waitlistinfo.WaitlistChildren.Count; i++)
                {
                    if (waitlistinfo.ChildrenPrioritizationVMs != null)
                    {
                        rm.updateWaitlistChild(waitlistinfo.WaitlistChildren[i].ChildId, waitlistinfo.WaitlistChildren[i].ChildName,
                            waitlistinfo.WaitlistChildren[i].DOB, waitlistinfo.WaitlistChildren[i].DateAdded, waitlistinfo.WaitlistChildren[i].ChildComments, waitlistinfo.ChildrenPrioritizationVMs[i]);
                    }
                    else
                    {
                        rm.updateWaitlistChild(waitlistinfo.WaitlistChildren[i].ChildId, waitlistinfo.WaitlistChildren[i].ChildName,
                            waitlistinfo.WaitlistChildren[i].DOB, waitlistinfo.WaitlistChildren[i].DateAdded);
                    }
                }
                return RedirectToAction("EditRoom", new { id = waitlistinfo.Room.room_id });
            }
        }

        [ActionName("CreateWaitlistChild")]
        public ActionResult CreateWaitlistChild(int? id = 1)
        {
            // I don't know if we need this but whatever
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            room room = db.rooms.Find(id);

            // stops facilities from modifying other facility's data
            if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);
            rm.RoomId = id;

            List<ChildRoster> rosterChildren = rm.getRosterChildren();

            List<ChildWaitlist> waitlistChildren = rm.getWaitlistedChildren();
            waitlistChildren.Add(new ChildWaitlist(0)); // add empty item to hold new waitlist child information

            childcare_facility facility = db.childcare_facility.Find(room.childcare_facility_id);

            SearchDAL EEDCalculator = new SearchDAL();
            DateTime EEDWithoutPrioritization = EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild());

            // to collect the facilities prioritizations
            List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();
            // to hold each prioritization
            List<PrioritizationEED> prioritizationEEDS = null;

            if (facilityPrioritizations.Count <= 0)
            {
                foreach (prioritization prioritization in facilityPrioritizations)
                {
                    prioritizationEEDS.Add(new PrioritizationEED(prioritization, EEDCalculator.getEED(facility, room.room_id, new AlgorithmChild(prioritization.priority_rank))));
                }
            }

            RoomVM roomInfo = new RoomVM(room, room.waitlists.First().waitlist_override_date, rosterChildren, waitlistChildren, facilityPrioritizations, EEDWithoutPrioritization, prioritizationEEDS);

            List<PrioritizationVM> childrenPrioritizationVMs = new List<PrioritizationVM>();

            List<int> selectedIds = new List<int>();
            for (var i = 0; i < waitlistChildren.Count - 1; i++)
            {
                selectedIds = new List<int>();
                var myFacilityPrioritizations = new SelectListItem[facilityPrioritizations.Count];

                for (var j = 0; j < myFacilityPrioritizations.Length; j++)
                {
                    prioritization thepr = waitlistChildren[i].Prioritizations.FirstOrDefault(item => item.prioritization_id == facilityPrioritizations[j].prioritization_id);

                    if (!(thepr == null))
                        selectedIds.Add(thepr.prioritization_id);
                }
                childrenPrioritizationVMs.Add(new PrioritizationVM()
                {
                    PrioritizationID = selectedIds.ToArray(),
                    Prioritizations = new MultiSelectList(facilityPrioritizations, "prioritization_id", "priority_name", selectedIds),
                    ChildId = waitlistChildren[i].ChildId
                });
            }
            selectedIds = new List<int>();
            childrenPrioritizationVMs.Add(new PrioritizationVM()
            {
                PrioritizationID = selectedIds.ToArray(),
                Prioritizations = new MultiSelectList(facilityPrioritizations, "prioritization_id", "priority_name", selectedIds),
                ChildId = 0
            });

            roomInfo.ChildrenPrioritizationVMs = childrenPrioritizationVMs;

            ViewBag.RoomId = id;

            return View(roomInfo);
        }

        [HttpPost]
        [ActionName("AddWaitlistChild")]
        public ActionResult AddWaitlistChild(RoomVM waitlistinfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            // stops facilities from modifying other facility's data
            else if (waitlistinfo.Room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                rm = new RoomDAL(waitlistinfo.Room.childcare_facility_id);
                rm.RoomId = waitlistinfo.Room.room_id;

                // if prioritizations exist for the facility
                if (waitlistinfo.ChildrenPrioritizationVMs != null && waitlistinfo.ChildrenPrioritizationVMs.Count == waitlistinfo.WaitlistChildren.Count)
                {
                    rm.createWaitlistChild(waitlistinfo.WaitlistChildren[waitlistinfo.WaitlistChildren.Count - 1].ChildName, waitlistinfo.WaitlistChildren[waitlistinfo.WaitlistChildren.Count - 1].DOB,
                        waitlistinfo.ChildrenPrioritizationVMs[waitlistinfo.WaitlistChildren.Count - 1]);
                }
                else
                {
                    rm.createWaitlistChild(waitlistinfo.WaitlistChildren[waitlistinfo.WaitlistChildren.Count - 1].ChildName, waitlistinfo.WaitlistChildren[waitlistinfo.WaitlistChildren.Count - 1].DOB);
                }


                return RedirectToAction("EditRoom", new { id = waitlistinfo.Room.room_id });
            }
        }

        public ActionResult DeleteWaitlistChild(int? id)
        {
            // don't really need this?
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            child child = db.children.Find(id);

            int roomId = child.room_id;

            room room = db.rooms.Find(roomId);

            if (child == null)
            {
                return HttpNotFound();
            }
            // stops facilities from modifying other facility's data
            else if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);

            rm.deleteWaitlistChild(child.child_id);

            return RedirectToAction("EditRoom", new { id = roomId });
        }

        public ActionResult MoveChildToRoster(int? id)
        {
            // don't really need this?
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            child child = db.children.Find(id);

            int roomId = child.room_id;

            room room = db.rooms.Find(roomId);

            if (child == null)
            {
                return HttpNotFound();
            }
            // stops facilities from modifying other facility's data
            else if (room.childcare_facility_id != (int)Session["CurrentFacilityId"])
            {
                return RedirectToAction("Error", "Home");
            }

            rm = new RoomDAL(room.childcare_facility_id);

            rm.moveWaitlistToRosterChild(child.child_id);

            return RedirectToAction("EditRoom", new { id = roomId });
        }

        [HttpPost]
        public ActionResult ValidateDateEqualOrGreater(DateTime Date)
        {
            if(Date == DateTime.Now && Date > DateTime.Now)
            {
                return Json(true);
            }
            return Json(false);
        }
    }
}