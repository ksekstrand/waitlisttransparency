﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WTSite.DAL;
using WTSite.Models;

namespace WTSite.Controllers
{
    public class SearchController : Controller
    {
        private SearchDAL sr = null;

        [HttpGet]
        public ActionResult SearchIndex()
        {
            // these are only used initially
            SearchResults searchCriteria = new SearchResults();
            searchCriteria.SearchCriteria = new SearchCriteria();
            searchCriteria.SearchCriteria.DOB = DateTime.Today;

            var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

            var myStatesArray = new SelectListItem[56];

            for (var i = 0; i < statesArray.Length; i++)
            {
                myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString() };
            }

            ViewBag.myStatesArray = myStatesArray;

            return View(searchCriteria);
        }

        // GET: Partial View for criteria input
        public ActionResult SearchCriteriaPartial()
        {
            return PartialView();
        }

        // POST: Search by City, State Criteria
        [HttpPost]
        [ActionName("SearchCityState")]
        public ActionResult SearchCityState(SearchCriteria searchcriteria)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                sr = new SearchDAL();

                // get all search results based on inputted criteria
                List<SearchResults> searchResults = sr.getSearchFacilitiesByCityState(new AlgorithmChild(searchcriteria.DOB), searchcriteria.City, searchcriteria.State);

                // stuff for list of states
                var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

                var myStatesArray = new SelectListItem[56];

                for (var i = 0; i < statesArray.Length; i++)
                {
                    myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString() };
                }

                ViewBag.myStatesArray = myStatesArray;

                // stuff for partial view
                SearchResults inputtedCriteria = new SearchResults();
                inputtedCriteria.SearchCriteria = searchcriteria;
                TempData["inputtedCriteria"] = inputtedCriteria;

                // hack for the search result markers to be displayed next to facility names
                string[] resultMarkers = new string[searchResults.Count];
                for (var i = 0; i < searchResults.Count(); i++)
                {
                    resultMarkers[i] = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (i + 1) + "|729EB3|FFFFFF";
                }

                ViewBag.ResultMarkers = resultMarkers;







                /////////////////////////////////test////////////////////////////////////
                //for (int i = 0; i < searchResults.Count; i++)
                //{
                //    // to hold each prioritization
                //    List<PrioritizationEED> prioritizationEEDS = new List<PrioritizationEED>();
                //    int priorityRank = 0;
                //    DateTime TempEED = new DateTime();

                //    SearchDAL EEDCalculator = new SearchDAL();
                //    rm = new RoomDAL(searchResults[i].Facility.childcare_facility_id);
                //    //rm = new RoomDAL(Facilities[i].childcare_facility_id);

                //    List<prioritization> facilityPrioritizations = rm.getFacilityPrioritizations();

                //    if (facilityPrioritizations.Count > 0)
                //    {
                //        foreach (prioritization prioritization in facilityPrioritizations)
                //        {
                //            priorityRank = prioritization.priority_rank;
                //            TempEED = EEDCalculator.getEED(searchResults[i].Facility, rooms[i].room_id, new AlgorithmChild(priorityRank));
                //            EEDsWithPrioritization.Add(new PrioritizationEED(prioritization, TempEED));
                //        }
                //    }
                //}
                /////////////////////////////////////test//////////////////////////////////



                return View(searchResults);
            }
        }

        // POST: Search by Zip Code Criteria
        [HttpPost]
        [ActionName("SearchZip")]
        public ActionResult SearchZip(SearchCriteria searchcriteria)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                sr = new SearchDAL();

                // get all search results based on inputted criteria
                List<SearchResults> searchResults = sr.getSearchFacilitiesByZip(new AlgorithmChild(searchcriteria.DOB), searchcriteria.ZipCode);

                // stuff for list of states
                var statesArray = new[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH",
                "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT",
                "WA", "WI", "WV", "WY"};

                var myStatesArray = new SelectListItem[56];

                for (var i = 0; i < statesArray.Length; i++)
                {
                    myStatesArray[i] = new SelectListItem() { Text = statesArray[i].ToString(), Value = statesArray[i].ToString() };
                }

                ViewBag.myStatesArray = myStatesArray;

                // stuff for partial view
                SearchResults inputtedCriteria = new SearchResults();
                inputtedCriteria.SearchCriteria = searchcriteria;
                TempData["inputtedCriteria"] = inputtedCriteria;

                // hack for the search result markers to be displayed next to facility names
                string[] resultMarkers = new string[searchResults.Count];
                for (var i = 0; i < searchResults.Count(); i++)
                {
                    resultMarkers[i] = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (i + 1) + "|729EB3|FFFFFF";
                }

                ViewBag.ResultMarkers = resultMarkers;

                return View(searchResults);
            }
        }
    }
}