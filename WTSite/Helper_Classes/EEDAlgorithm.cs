﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTSite.Models;

namespace WTSite.Helper_Classes
{
    public class EEDAlgorithm
    {
        public EEDAlgorithm() { }

        //public List<DateTime> calculateEstimatedEnrollmentDateList(int capacity, int roomAgeMax, List<AlgorithmChild> roster, List<AlgorithmChild> tempWaitlist, AlgorithmChild CNC)
        //{
        //    List<DateTime> _childcareList = new List<DateTime>();
        //    DateTime returnable = new DateTime();
        //    foreach(AlgorithmChild c)
        //    calculateEstimatedEnrollmentDate(capacity, roomAgeMax, roster, tempWaitlist, CNC);



        //    return _childcareList;
        //}

        // needs to return DateTime later, rather than void
        public DateTime calculateEstimatedEnrollmentDate(int capacity, int roomAgeMax, List<AlgorithmChild> roster, List<AlgorithmChild> tempWaitlist, 
            AlgorithmChild CNC)
        {
            DateTime EED = DateTime.Today; // contains the EED, if position is automatically available (i.e. positions already open), then the date is today
            List<AlgorithmChild> waitlist = new List<AlgorithmChild>();

            // if tempWaitlist is not null, insert CNC into appropriate waitlist position
            if (tempWaitlist.Count > 0)
            {
                // order the waitlist by PrioritizationRank, dateAdded doesn't matter in this context
                waitlist = tempWaitlist.OrderBy(item => item.PrioritizationRank).ToList();

                // returns the index needed to insert the child
                int insertIndex = returnPosition(waitlist, CNC, _adjust);

                // insert CNC into appropriate waitlist position
                waitlist.Insert(insertIndex, CNC);
            }
            // else if tempWaitlist is null, just add CNC to waitlist, no extra drama needed
            else
            {
                waitlist.Add(CNC);
            }

            // if the roster is not null, populate the LeaveDate property of all child objects in both the roster and waitlist (waitlist will not be null, because of the CNC)
            if (roster != null)
            {
                foreach (AlgorithmChild child in roster)
                {
                    child.calculateAgingOut(roomAgeMax);
                }
            }
            else
            {
                // if the roster is null, initialize roster
                roster = new List<AlgorithmChild>();
            }

            foreach (AlgorithmChild child in waitlist)
            {
                child.calculateAgingOut(roomAgeMax);
            }

            // calculate EED of the CNC
            // while the CNC exists in the waitlist, loop through until CNC is on the roster (no longer on the waitlist)
            while (waitlist.Exists(item => item.ChildID == CNC.ChildID))
            {
                // calculate the room's next available opening
                // if the roster is not filled or null, a position is available
                if (roster.Count < capacity)
                {
                    // add top waitlisted child to roster
                    roster.Add(waitlist[0]);

                    // remove waitlisted child from waitlist
                    waitlist.Remove(waitlist[0]);

                }
                // else the roster is filled, position needs to be made available
                else
                {
                    // determine who ages out first on the roster by who has the minimum LeaveDate
                    DateTime minDate = DateTime.MaxValue;   // holds lowest date value
                    int childIndex = -1;                    // index of the child with the lowest date value, default is -1 because it will throw an error if not changed
                    int currentIndex = 0;                   // current index, used in foreach loop

                    foreach (AlgorithmChild child in roster)
                    {
                        if (child.LeaveDate < minDate)
                        {
                            minDate = child.LeaveDate;
                            childIndex = currentIndex;
                        }
                        currentIndex++;
                    }
                    var a = roster[childIndex].ChildID;
                    // NOTE: issue discovered and resolved, need to get the highest leave date, not just the last person to leave before the CNC's date
                    if (roster[childIndex].LeaveDate > EED)
                    {
                        // this is the EED at this point
                        EED = roster[childIndex].LeaveDate;
                    }

                    // remove child from roster
                    roster.RemoveAt(childIndex);

                    if (roster.Count < capacity)
                    {
                        // add top waitlisted child to roster
                        roster.Add(waitlist[0]);

                        // remove waitlisted child from waitlist
                        waitlist.Remove(waitlist[0]);
                    }
                }
            }
            // return date the CNC no longer exists in the waitlist (they were moved to the roster) as the EED
            return EED;
        }

        // global variable to store adjustment
        int _adjust = 0;

        // determines the position to insert the CNC
        int returnPosition(List<AlgorithmChild> waitlist, AlgorithmChild CNC, int adjust)
        {
            // determines the last position of the last instance of the CNC's PrioritizationRank minus the adjustment amount
            int insertIndex = waitlist.LastIndexOf(waitlist.Where(item => item.PrioritizationRank == CNC.PrioritizationRank - adjust).LastOrDefault());

            // else if the the index never exists 
            if (CNC.PrioritizationRank - _adjust == 0)
            {
                // reset integer value to 0
                _adjust = 0;

                // place CNC at the beginning of the list
                return 0;
            }
            // else if the the index doesn't exist  
            else if (insertIndex == -1)
            {
                // increase global adjustment value to 0
                _adjust++;

                return returnPosition(waitlist, CNC, _adjust);
            }
            // reset global adjustment value to 0
            _adjust = 0;

            return insertIndex + 1;
        }
    }
}